import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseListPaging } from '../models/common-response/response-list';
import { AssignRequest } from '../models/procurement/assign-request';
import { Employee } from '../models/procurement/employee';
import { GuidelineMenuNav } from '../models/procurement/guideline-menu';
import { Lov } from '../models/procurement/lov';
import { Project } from '../models/procurement/project';
import { SearchErpProjectResponse } from '../models/project-mst/erp-project';
import { environment } from '../../environments/environment';
import moment, { Moment } from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  readonly mdsUrl = `${environment.apiAuthenticateURL}`;
  readonly projectUrl = `${environment.apiAuthenticateURL}`;

  private guidelineLoader = new BehaviorSubject<GuidelineMenuNav[]>([]);
  public readonly guidelineLoading = this.guidelineLoader.asObservable();

  private guidelineMenuNavLoader = new BehaviorSubject<boolean>(true);
  public readonly guidelineMenuNavLoading = this.guidelineMenuNavLoader.asObservable();

  endpoint = {
    getProjectCategoryUrl: this.mdsUrl + '/lov/projectCategory',
    getProjectTypeUrl: this.mdsUrl + '/lov/projectType',
    getProjectCharacterUrl: this.mdsUrl + '/lov/projectCharacter',
    getProjectCharacterTypeUrl: this.mdsUrl + '/lov/projectCharacterType',
    getProjectMethodUrl: this.mdsUrl + '/lov/projectMethod',
    getProjectMethodConUrl: this.mdsUrl + '/lov/projectMethodCon',
    getPurchaseTypeUrl: this.mdsUrl + '/lov/purchaseType',
    getCurrenciesUrl: this.mdsUrl + '/lov/apps/currencies',
    getErpProjectsUrl: this.mdsUrl + '/lov/apps/erpProjects',
    getAgreementTypeUrl: this.mdsUrl + '/lov/apps/agreementType',
    getAccountSegmentsUrl: this.mdsUrl + '/lov/apps/accountSegments',
    getIsProcureUrl: this.mdsUrl + '/lov/apps/isProcure',
    getEmployeeUrl: this.mdsUrl + '/lov/apps/employee',
    searchProjectBudgetUrl: this.mdsUrl + '/projectBudget',
    searchErpProjectrl: this.mdsUrl + '/lov/search/erpproject',

    searchSegment1CostcenterUrl: this.mdsUrl + '/lov/apps/segment1Costcenter',
    searchSegment2BudgetSourceUrl: this.mdsUrl + '/lov/apps/segment2BudgetSource',
    searchSegment3AccountUrl: this.mdsUrl + '/lov/apps/segment3Account',
    searchSegment4BudgetTypeUrl: this.mdsUrl + '/lov/apps/segment4BudgetType',
    searchSegment5BudgetUrl: this.mdsUrl + '/lov/apps/segment5Budget',
    findSegmentDefaultUrl: this.mdsUrl + '/lov/apps/segmentDefault',
    findAccountSegmentsCostcenterUrl: this.mdsUrl + '/lov/apps/accountSegmentsCostcenter',


    getProjectMasterUrl: this.projectUrl + '/v1',
    saveProjectMasterUrl: this.projectUrl + '/v1',
    updateProjectMasterUrl: this.projectUrl + '/v1/update',
    searchProjectMasterUrl: this.projectUrl + '/v1/search',
    freezeProjectMasterUrl: this.projectUrl + '/v1/freeze',
    cancelProjectMasterUrl: this.projectUrl + '/v1/cancel',
    assignProjectAdministratorUrl: this.projectUrl + '/v1/assign',
    searchProjectUrl: this.projectUrl + '/v1',

    getGuidelineUrl: this.mdsUrl + '/guideline',
    getGuidelineMenuUrl: this.mdsUrl + '/guideline/menu',

    uploadFileUrl: this.projectUrl + "/document/uploads",
    downloadFileUrl: this.projectUrl + "/document/download",
    getListFileUrl: this.projectUrl + "/document/files",
    deleteFileUrl: this.projectUrl + "/document/delete",
  }

  projectCategoryList: any;
  constructor(private http: HttpClient) { }

  getProjectMasterById(projectCode: string): Observable<Project> {
    return this.http.get<any>(this.endpoint.getProjectMasterUrl + `/${projectCode}`).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }

  getProjectCategoryList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getProjectCategoryUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getProjectTypeList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getProjectTypeUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getProjectCharacterList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getProjectCharacterUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getProjectCharacterTypeList(characterCode: string = ''): Observable<Lov[]> {
    characterCode = characterCode !== '' ? '/' + characterCode : characterCode;
    return this.http.get<any>(this.endpoint.getProjectCharacterTypeUrl + characterCode).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getProjectMethodListByCategoryCode(categoryCode: string = ''): Observable<Lov[]> {
    categoryCode = categoryCode !== '' ? '/' + categoryCode : categoryCode;
    return this.http.get<any>(this.endpoint.getProjectMethodUrl + categoryCode).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getPurchaseTypeList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getPurchaseTypeUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getCurrenciesList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getCurrenciesUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getErpProjectsList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getErpProjectsUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getAgreementTypeList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getAgreementTypeUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getAgreementTypeListByCode(code: String): Observable<Lov> {
    let codetmp = code
    code = code !== '' ? '/' + code : code;
    return this.http.get<any>(this.endpoint.getAgreementTypeUrl + code).pipe(
      map((affectRows: any) => {
        let data = affectRows.data.find((item:any) => item.code == codetmp)
        // return affectRows.data[0];
        return data;
      })
    );
  }
  getAccountSegmentsList(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getAccountSegmentsUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getIsProcure(): Observable<Lov[]> {
    return this.http.get<any>(this.endpoint.getIsProcureUrl).pipe(
      map((affectRows: any) => {
        return affectRows.data;
      })
    );
  }
  getCostCenterByCostCenterCode(costCenterCode: string): Observable<Lov> {
    costCenterCode = costCenterCode !== '' ? '/' + costCenterCode : costCenterCode;
    return this.http.get<any>(this.endpoint.getAccountSegmentsUrl + costCenterCode).pipe(
      map((affectRows: any) => {
        return affectRows.data[0];
      })
    );
  }

  saveProject(body: any) {
    return this.http.post<any>(this.endpoint.saveProjectMasterUrl, body).pipe(
      map((affectRows: any) => {
        return affectRows.data
      })
    );
  }

  updateProject(body: any) {
    return this.http.post<any>(this.endpoint.updateProjectMasterUrl + `/${body.projectCode}`, body).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  searchProject(params: any, pageNo: number, pageSize: number): Observable<any> {
    let budgetYear: Moment = moment(params.budgetYear);
    console.log(budgetYear)
    const parameters = new HttpParams()
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize)
      .set('projectCode', params.projectCode)
      .set('projectBudgetCode', params.projectBudgetCode)
      .set('projectCategoryCode', params.projectCategoryCode)
      .set('budgetYear', (params.budgetYear === null || params.budgetYear === undefined || params.budgetYear === '' || budgetYear.toString() === '') ? '' : budgetYear.toISOString());
    return this.http.get(this.endpoint.searchProjectMasterUrl, { params: parameters }).pipe(
      map((affectRows: ResponseListPaging) => {
        if (affectRows.header && affectRows.header.code === "2000") {
          if (affectRows.data!.length > 0) {
            return { data: affectRows.data, paging: affectRows.paging }
          }
          throw new Error("ไม่พบข้อมูล");
        } else if (affectRows.header && affectRows.header.code === "2001") {
          throw new Error("ไม่พบข้อมูล");
        }
        throw new Error(affectRows.header!.desc);
      })
    );
  }

  deleteProject(projectCode: string) {
    return this.http.delete<any>(this.endpoint.getProjectMasterUrl + `/${projectCode}`).pipe(
      map((affectRows: any) => {
        return affectRows;
      })
    );
  }

  freezeProject(projectCode: string) {
    return this.http.post<any>(this.endpoint.freezeProjectMasterUrl + `/${projectCode}`, {}).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  getGuidelineLevel1(projectCode: string) {
    return this.http.get<any>(this.endpoint.getGuidelineUrl + `/level-1/${projectCode}`).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  getGuidelineLevel2(projectCode: string) {
    return this.http.get<any>(this.endpoint.getGuidelineUrl + `/level-2/${projectCode}`).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  getEmployeeByCostCenter(costCenter: string): Observable<Employee[]> {
    return this.http.get<any>(this.endpoint.getEmployeeUrl + `/${costCenter}`).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  reloadGuidelineMenu(projectCode: string): void {
    this.guidelineMenuNavLoader.next(false)
    this.http.get<any>(this.endpoint.getGuidelineMenuUrl + `/${projectCode}`)
      .subscribe(
        (affectRows: any) => {
          if (affectRows.header.code == 2000) {
            this.guidelineMenuNavLoader.next(true)
            this.guidelineLoader.next(affectRows.data.navMenu);
          }
          throw new Error(affectRows.header.desc);
        }
      )
  }

  assignProjectAdministrator(request: AssignRequest): Observable<any> {
    return this.http.post<any>(this.endpoint.assignProjectAdministratorUrl + `/${request.projectCode}`, request).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  searchProjectMasterByCodeAndName(searchValue: string, pageNo: number, pageSize: number) {
    const params = new HttpParams()
      .set('projectCode', searchValue)
      .set('projectName', searchValue)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.searchProjectUrl, { params }).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }
  searchProjectBudgetByCodeAndName(searchValue: string, pageNo: number, pageSize: number) {
    const params = new HttpParams()
      .set('projectCode', searchValue)
      .set('projectName', searchValue)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.searchProjectBudgetUrl, { params }).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  uploadFile(files: File[], projectCode: string): Observable<any> {
    let formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append("file", files[i]);
    }
    formData.append("projectCode", projectCode);
    return this.http.post<any>(this.endpoint.uploadFileUrl, formData).pipe(
      map((affectRows: any) => {
        return affectRows.data
      })
    );
  }

  downloadFile(docHeaderId: number): Observable<Blob> {
    let params = new HttpParams().set('docHeaderId', docHeaderId);
    return this.http.get(this.endpoint.downloadFileUrl, {
      responseType: 'blob',
      params,
    });
  }

  deleteFile(docHeaderId: number): Observable<any> {
    return this.http.post<any>(this.endpoint.deleteFileUrl, { docHeaderId }).pipe(
      map((affectRows: any) => {
        return affectRows.data
      })
    );
  }

  getDocumentList(projectCode: string): Observable<any> {
    return this.http.get<any>(this.endpoint.getListFileUrl + '/' + projectCode).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.code);
      })
    );
  }

  cancelProject(projectCode: string) {
    return this.http.post<any>(this.endpoint.cancelProjectMasterUrl + `/${projectCode}`, {}).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  searchErpProject(searchKey: any, pageNo: number, pageSize: number): Observable<SearchErpProjectResponse> {
    const params = new HttpParams()
      .set('searchKey', searchKey)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<SearchErpProjectResponse>(this.endpoint.searchErpProjectrl, { params }).pipe(
      map((affectRows: SearchErpProjectResponse) => {
        if (affectRows.header.code && affectRows.header.code === '2000') {
          return affectRows;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }

  searchSegment1CostcenterByCodeAndName(searchKey: string, pageNo: number, pageSize: number): Observable<any> {
    const params = new HttpParams()
      .set('searchKey', searchKey)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.searchSegment1CostcenterUrl, { params: params }).pipe(
      map((affectRows: any) => {
        console.log('affectRows.data :: ',affectRows.data);
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        return new Error(affectRows.header.code)
      })
    );
  }

  searchSegment2BudgetSourceByCodeAndName(searchKey: string, pageNo: number, pageSize: number): Observable<any> {
    const params = new HttpParams()
      .set('searchKey', searchKey)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.searchSegment2BudgetSourceUrl, { params: params }).pipe(
      map((affectRows: any) => {
        console.log('affectRows.data :: ',affectRows.data);
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        return new Error(affectRows.header.code)
      })
    );
  }

  searchSegment3AccountByCodeAndName(searchKey: string, pageNo: number, pageSize: number): Observable<any> {
    const params = new HttpParams()
      .set('searchKey', searchKey)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.searchSegment3AccountUrl, { params: params }).pipe(
      map((affectRows: any) => {
        console.log('affectRows.data :: ',affectRows.data);
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        return new Error(affectRows.header.code)
      })
    );
  }
  
  searchSegment4BudgetTypeByCodeAndName(searchKey: string,budgetTypeDefault: string, pageNo: number, pageSize: number): Observable<any> {
    const params = new HttpParams()
      .set('searchKey', searchKey)
      .set('budgetTypeCode', budgetTypeDefault)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.searchSegment4BudgetTypeUrl, { params: params }).pipe(
      map((affectRows: any) => {
        console.log('affectRows.data :: ',affectRows.data);
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        return new Error(affectRows.header.code)
      })
    );
  }
  
  searchSegment5BudgetByCodeAndName(searchKey: string, pageNo: number, pageSize: number): Observable<any> {
    const params = new HttpParams()
      .set('searchKey', searchKey)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.searchSegment5BudgetUrl, { params: params }).pipe(
      map((affectRows: any) => {
        console.log('affectRows.data :: ',affectRows.data);
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        return new Error(affectRows.header.code)
      })
    );
  }

  findSegmentDefault( employeeID:string,  costCenterCode:string,  itemCate:string,  pItemNumber:string,  projectNumber:string,  pCombineOnly:string): Observable<any> {
    const params = new HttpParams()
      .set('employeeID', employeeID)
      .set('costCenterCode', costCenterCode)
      .set('itemCate', itemCate)
      .set('pItemNumber', pItemNumber)
      .set('projectNumber', projectNumber)
      .set('pCombineOnly', pCombineOnly)
    return this.http.get<any>(this.endpoint.findSegmentDefaultUrl, { params: params }).pipe(
      map((affectRows: any) => {
        if (affectRows.header.code == 2000) {
          console.log('findSegmentDefault :: ',affectRows.data);
          return affectRows.data;
        }
        throw new Error(affectRows.header.desc);
      })
    );
  }  
  
  searchAccountSegmentsCostcenterByCodeAndName(searchKey: string, pageNo: number, pageSize: number): Observable<any> {
    const params = new HttpParams()
      .set('searchKey', searchKey)
      .set('pageNo', pageNo + 1)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.endpoint.findAccountSegmentsCostcenterUrl, { params: params }).pipe(
      map((affectRows: any) => {
        console.log('affectRows.data :: ',affectRows.data);
        if (affectRows.header.code == 2000) {
          return { data: affectRows.data, paging: affectRows.paging }
        }
        return new Error(affectRows.header.code)
      })
    );
  }
  getProjectMethodConditionByProjectMethodCode(projectMethodCode: string = ''): Observable<any> {
    const params = new HttpParams()
      .set('projectMethodCode', projectMethodCode) 
    return this.http.get<any>(this.endpoint.getProjectMethodConUrl, { params: params }).pipe(
      map((affectRows: any) => {
        console.log('affectRows.data :: ',affectRows.data);
        if (affectRows.header.code == 2000) {
          return affectRows.data
        }
        return new Error(affectRows.header.code)
      })
    );
  }
}
