import { Injectable, Injector } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { employeeResponse } from '../../models/Response/Employee/employeeResponse';
import { environment } from '../../../environments/environment';
import { UserInfo } from '../../models/userinfo';

import { CostcenterDetailResponse } from '../../models/Response/CostcenterDetailResponse/CostcenterDetailResponse';

import { version } from '../../models/version';
import { getAllHRResponse } from '../../models/Response/getAllHR/getAllHRResponse';
import { getUserRole } from '../../models/Response/getUserRole/getUserRole';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private currentUserSubject: BehaviorSubject<UserInfo>;
  public currentUser: Observable<UserInfo>;
  private readonly CURRENT_USER = 'currentUser';
  //private loggedUser: string;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<UserInfo>(
      JSON.parse(localStorage.getItem(this.CURRENT_USER) || '{}')
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): UserInfo {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http
      .post<UserInfo>(`${environment.apiAuthenticateURL}/authenticate`, {
        username,
        password,
      })
      .pipe(
        map((user) => {
          this.isTokenExpired(user);
          localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
          this.currentUserSubject.next(user);
          return user;
        })
      );
  }

  getUserDetailLogin(username: string, role: string) {
    return this.http
      .get<any>(`${environment.apiAuthenticateURL}/v1/employee/get/${username}`)
      .pipe(
        map((res) => {
          var user = new UserInfo();
          user = res.data;
          user.username = user.emplid;
          user.rankCode = user.rankCode;
          user.rankdescription = user.rankDescr;
          user.costcenter = user.costCenterCode;
          user.role = role;
          user.token = this.getAuthToken();
          localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
          this.currentUserSubject.next(user);
          return user;
        })
      );
  }
  logout() {
    localStorage.clear();
    this.currentUserSubject.next(new UserInfo());
  }

  getJwtToken() {
    return localStorage.getItem(this.CURRENT_USER);
  }

  isLoggedIn() {
    return !this.currentUser;
  }

  isTokenExpired(response: any) {
    if (response.status === 401) {
      var refreshResponse = null;
      this.refreshToken().subscribe((data) => {
        refreshResponse = data;
        localStorage.setItem(
          this.CURRENT_USER,
          JSON.stringify(refreshResponse)
        );
        this.currentUserSubject.next(refreshResponse);
        return refreshResponse;
      });

      if (!refreshResponse) {
        return response; //failed to refresh so return original 401 response
      }
    } else {
      //status is not 401 and/or there's no Token-Expired header
      return response; //return the original 401 response
    }
  }

  getRefreshToken() {
    var currentData = new BehaviorSubject<UserInfo>(
      JSON.parse(localStorage.getItem(this.CURRENT_USER) || '{}')
    );
    return currentData.value.refresh_token;
  }

  refreshToken(): Observable<UserInfo> {
    var currentData = new BehaviorSubject<UserInfo>(
      JSON.parse(localStorage.getItem(this.CURRENT_USER) || '{}')
    );
    return this.http
      .post<UserInfo>(
        `${environment.apiAuthenticateURL}/refreshtoken`,
        currentData.value
      )
      .pipe(
        map((user) => {
          if (user && user.token) {
            localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
          }
          return <UserInfo>user;
        })
      );
  }

  getAuthToken(): string {
    let currentUser = JSON.parse(
      localStorage.getItem(this.CURRENT_USER) || '{}'
    );
    if (currentUser != null) {
      return currentUser.token;
    }
    return '';
  }

  getUserDetail(username: string) {
    return this.http
      .get<employeeResponse>(
        `${environment.apiAuthenticateURL}/hrms/employeeDetail/${username}`
      )
      .pipe(
        map((res) => {
          return res;
        })
      );
  }

  getCostCenterDetail(costcenter: string) {
    return this.http
      .get<CostcenterDetailResponse>(
        `${environment.apiAuthenticateURL}/hrms/getCostCenterDetail/${costcenter}`
      )
      .pipe(
        map((res) => {
          return res;
        })
      );
  }

  getUserRole(username: string) {
    return this.http
      .get<getUserRole>(
        `${environment.apiAuthenticateURL}/v1/role/roleUser/${username}`
      )
      .pipe(
        map((res) => {
          return res;
        })
      );
  }

  getAllHR(username: string, costcenter: string) {
    return this.http
      .get<getAllHRResponse>(
        `${environment.apiAuthenticateURL}/hrms/getAllHR/${username}/${costcenter}`
      )
      .pipe(
        map((res) => {
          return res;
        })
      );
  }

  serviceVersion() {
    return this.http
      .get<version>(`${environment.apiAuthenticateURL}/version`)
      .pipe(
        map((res) => {
          return res;
        })
      );
  }
}
