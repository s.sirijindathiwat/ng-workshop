
import { UserInfo } from './models/userinfo';
import { Component, ViewChild, ElementRef, HostListener, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { AuthService } from './services/login/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'P2PSubsystemWeb';

  opened = true;
  @ViewChild('sidenav') sidenav!: MatSidenav; 

  currentUser: UserInfo = new UserInfo();
  isLogin = false;

  constructor(private router: Router, private authenService: AuthService) {
    this.authenService.currentUser.subscribe(x => this.currentUser = x);
    this.currentUser = this.authenService.currentUserValue;

    console.log("currentUser : " , this.currentUser);
    console.log("currentUser access_token: " + this.currentUser.token);
  }

  ngOnInit() {
    if (this.currentUser.token) { 
      this.isLogin = true;
      console.log("isLogin : " + this.isLogin);
    } else {
      console.log("isLogin : " + this.isLogin);
    }
  }
}
