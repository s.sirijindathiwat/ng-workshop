import { Injectable } from "@angular/core";
import { ProjectDocFile } from "../components/project/upload-download-document/upload-download-document.component";
import { faFileIcon, faIcon, FileData } from "../models/file/file";
import { GlobalFile } from "../models/workflow/global-file";

@Injectable({ providedIn: 'root' })
export class FileIconUtil {
    setFileListIcon(files: FileData[]) {
        let fileList = files.map((_file: FileData) => {
            let faIconClass = faFileIcon.filter((_faIcon: faIcon) => _faIcon.mimeType.includes(_file.type));
            _file.icon = faIconClass.length > 0 ? faIconClass[0].icon : "fa-file-alt"
            return _file;
        })
        return fileList;
    }
    setFileIcon(file: FileData) {
        let faIconClass = faFileIcon.filter((_faIcon: faIcon) => _faIcon.mimeType.includes(file.type));
        file.icon = faIconClass.length > 0 ? faIconClass[0].icon : "fa-file-alt"
        return file;
    }

    setGlobalFileListIcon(files: GlobalFile[]): GlobalFile[] {
        let fileList = files.map((_file: GlobalFile) => {
            let faIconClass = faFileIcon.filter((_faIcon: faIcon) => _faIcon.mimeType.includes(_file.fileType));
            _file.icon = faIconClass.length > 0 ? faIconClass[0].icon : "fa-file-alt"
            return _file;
        })
        return fileList;
    }

    setGlobalFileIcon(file: GlobalFile): GlobalFile {
        let faIconClass = faFileIcon.filter((_faIcon: faIcon) => _faIcon.mimeType.includes(file.fileType));
        file.icon = faIconClass.length > 0 ? faIconClass[0].icon : "fa-file-alt"
        return file;
    }

    setProjectFileListIcon(files: ProjectDocFile[]): ProjectDocFile[] {
        let fileList = files.map((_file: ProjectDocFile) => {
            let faIconClass = faFileIcon.filter((_faIcon: faIcon) => _faIcon.mimeType.includes(_file.fileType));
            _file.icon = faIconClass.length > 0 ? faIconClass[0].icon : "fa-file-alt"
            return _file;
        })
        return fileList;
    }
}