import { Injectable } from '@angular/core'; 
import { FlexValue } from '../models/dropdownlist/FlexValue'; 
import { ToastrService } from "ngx-toastr";  

@Injectable({ providedIn: 'root' })
export class utility_helper {

  public static BT_TXT_YES = "ใช่";
  public static BT_TXT_NO = "ไม่";
  public static BT_TXT_OK = "ตกลง";
  public static BT_TXT_CANCEL = "ยกเลิก";
  public static BT_TXT_YES_DELETE = "ใช่,ต้องการลบ";
  public static BT_TXT_NO_DELETE = "ไม่,ยกเลิก";
  public static BT_TXT_CONFIRM = "ยืนยัน";


  public static TITLE_SAVE = "บันทึก!";
  public static TITLE_WARNING = "คำเตือน!";
  public static TITLE_DELETE = "ลบ!";
  public static TITLE_ERROR = "พบข้อผิดพลาด!";

  public static LABEL_IT_PROJECT = " IT Project ";
  public static LABEL_NON_IT_PROJECT = "Non IT Project ";

  public static MSG_CONFIRM_PROCESS = "การยืนยันข้อมูลการดำเนินการใช่หรือไม่?!";
  public static MSG_CONFIRM_DELETE_YES_OR_NO = "คุณต้องการลบรายการนี้ใช่หรือไม่?!"

  public static MSG_SAVE_SUCCESS = "บันทึกข้อมูลเรียบร้อยแล้ว.";
  public static MSG_SAVE_NOT_SUCCESS = "บันทึกข้อมูลไม่สำเร็จ!!!";
  public static MSG_DELETE_SUCCESS = "ลบรายการเรียบร้อยแล้ว.";

  public static MSG_SAVE_CREATE_PROJECT_SUCCESS = "บันทึกเริ่มดำเนินโครงการเรียบร้อยแล้ว.";
  public static MSG_SAVE_CREATE_PROJECT_AND_DO_UPDATE = "บันทึกเริ่มดำเนินโครงการเรียบร้อยแล้ว ต้องการบันทึกแผนงานโครงการหรือไม่.";
  public static MSG_NO_DATA_FOUND = "No data found";

  public static MSG_ALREADY_HAS_PROJECT = "มีการบันทึกเริ่มโครงการแล้วหากต้องการแก้ไขให้เลือกโครงการที่อยู่ระหว่างดำเนินการและกดปุ่มบันทึกความคืบหน้า";
  public static MSG_PLEASE_SELECT_TASK = "กรุณาเลือก Project Task อย่างน้อย 1 Task";
  public static MSG_WARNING_REQUEST_BUGDET_MORETHAN2M = "งบประมาณที่ขออนุมัติใช้งานรวมกันทุก Task มากกว่า 2 ล้านบาท ไม่สามารถเลือก จำนวนเงินงบประมาณที่ขออนุมัติ ไม่เกิน 2 ล้านบาทได้";

  public static FROM_PAGE_OPEN_CLOSE = "OC";
  public static FROM_PAGE_CONFIRM_PROJECT = "CP";
  public static FROM_PAGE_REPORT = "RPT";
  public static FROM_PAGE_PROJECT_STATUS = "PS";

  public static EDIT_PLAN_DATE = "มีการบันทึก Actual แล้ว หากต้องการปรับแผนระบบจะเคลียร์ Actual";
  public static DATE_LESS_THAN = "วันที่เริ่มโครงการต้องน้อยกว่าวันที่สิ้นสุดโครงการ";

  public static TASK_NAME_START = "เริ่มโครงการ";
  public static TASK_NAME_REVIEW = "IT PROJECT REVIEW";
  public static TASK_NAME_APPROVE = "อนุมัติโครงการ";
  public static TASK_NAME_PROCURE = "จัดซื้อจัดจ้าง";
  public static TASK_NAME_DEVELOPMENT = "PROJECT DEVELOPMENT";
  public static TASK_NAME_EXAMINE = "PROJECT EXAMINE";

  public static CHECK_DATE_START = "วันที่ดำเนินการแล้วเสร็จต้องมากกว่าวันที่เริ่มดำเนินการ";
  public static CHECK_DATE_DELIVER = "วันที่ตรวจรับต้องมากกว่าวันที่ส่งมอบ";
  public static CHECK_DATE_RD = "วันที่อนุมัติเบิกจ่ายต้องมากกว่าวันที่ขอเบิกจ่าย";
  public static CHECK_DELAY = "วันที่ Actual ห้าม < วันที่ Plan date กรุณาปรับแผน";

  public static toastr: ToastrService;  

  formatDateToStringDDMMYYYY_TH_TIME(date: Date): string {
    if (date == null)
      return '';

    date = new Date(date);

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year;
    if (date.getFullYear() < 2500)
      year = date.getFullYear() + 543;
    else
      year = date.getFullYear();
    let time = date.toLocaleTimeString();
    return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year + " " + time;
  }

  formatDateToStringDDMMYYYY_TH(date: Date): string {
    if (date == null)
      return '';

    date = new Date(date);

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year;
    if (date.getFullYear() < 2500)
      year = date.getFullYear() + 543;
    else
      year = date.getFullYear();

    return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
  }

  formatDateToStringDDMMYYYY_THMonth(date: Date): string {
    if (date == null)
      return '';

    date = new Date(date);

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year;
    if (date.getFullYear() < 2500)
      year = date.getFullYear() + 543;
    else
      year = date.getFullYear();

    return this._to2digit(month) + '/' + year;
  }

  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }

  formatYear_TH(year: any): string {
    if (year == null || year == '')
      return '';

    if (parseFloat(year) < 2500)
      return (parseFloat(year) + 543).toString();
    else
      return year.toString();
  }

  formatYear_EN(year: any): string {
    if (year == null || year == '')
      return '';

    if (parseFloat(year) < 2500)
      return (parseFloat(year) - 543).toString();
    else
      return year.toString();
  }

  listDdlYear() {
    let arr = [];
    let year = new Date().getFullYear() - 5;//แสดงข้อมูลปีย้อนหลัง 5 ปี    

    for (let i = 0; i < 10; i++) {
      let v = new FlexValue;
      v.flex_value = (year + i).toString();
      v.description = this.formatYear_TH(year + i);
      arr.push(v);
    }
    return arr;
  }

  listDdlYearUserList() {
    let arr = [];
    let year = new Date().getFullYear() - 9;//แสดงข้อมูลปีย้อนหลัง 10 ปี    

    for (let i = 0; i < 10; i++) {
      let v = new FlexValue;
      v.flex_value = (year + i).toString();
      v.description = this.formatYear_TH(year + i);
      arr.push(v);
    }
    return arr;
  } 

  public static showSuccess(message: string | undefined, title: string | undefined) {
    this.toastr.success(message, title)
  }

  public static showError(message: string | undefined, title: string | undefined) {
    this.toastr.error(message, title)
  }

  public static showInfo(message: string | undefined, title: string | undefined) {
    this.toastr.info(message, title)
  }

  public static showWarning(message: string | undefined, title: string | undefined) {
    this.toastr.warning(message, title)
  }

  constructor(private toastr: ToastrService) { }

  showErrortoastr(message: string | undefined, title: string | undefined) {
    this.toastr.error(message, title);
  }

  showSuccesstoastr(message: string | undefined, title: string | undefined) {
    this.toastr.success(message, title);
  }

  
  public static popupError(msg: string) {
    this.toastr.warning(msg, "Warning");
    
    
  }

  
}
