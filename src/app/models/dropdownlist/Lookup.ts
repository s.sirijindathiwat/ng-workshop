export class Lookup {
    enabledFlag!: string;
    endDate!: Date;
    lookupCode!: string;
    lookupDescription!: string;
    lookupId!: number;
    lookupOrder!: number;
    lookupType!: string;
    meaning!: string;
    startDate!: string;
  }
  