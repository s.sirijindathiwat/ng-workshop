export class result{
  citizenID! : string
  costCenterCode! : string
  costCenterDescr! :	string
  deptCode!	: string
  deptDescr!	: string
  email! : string
  emplid! : string
  firstName! : string
  lastName! : string
  location! : string
  phone! : string
  rankCode! : string
  rankDescr! : string
  responseCode! : string
  responseDescr! : string
  titleThai! : string
  startDate! : Date
  endDate! : Date

  ownerid! : string
  ownername! : string
  ownerrankDescr! : string
  ownermail! : string
  }

