import { UserRole } from './UserRole';

export class UserInfo {
  username!: string;
  password!: string;
  message!: string;
  roleid!: string;
  medroleid!: string;
  rolenameth!: string;
  rolenameen!: string;
  name!: string;
  costcenter!: string;
  headquartercostcenter!: string;
  email!: string;
  emplid!: string;
  titleThai!: string;
  firstName!: string;
  lastName!: string;
  position!: string;
  rankCode!: string;
  rankDescr!: string;
  rankdescription!: string;
  deptCode!: string;
  deptDescr!: string;
  location!: string;
  costCenterCode!: string;
  costCenterDescr!: string;
  region4!: string;
  group2!: string;
  businessUnit1!: string;
  success!: boolean;
  access_token!: string;
  refresh_token!: string;
  userrolepermission!: UserRole;
  token!: string;
  role!: string;
}
