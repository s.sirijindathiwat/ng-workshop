export interface AnsRequestInfo {
    requestType: boolean;
    employeeCode: string;
    fullname: string;
    remarks: string;
    additionalDoc?: boolean;
}