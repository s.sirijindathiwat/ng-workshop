export interface GlobalFile {
  formId?: number;
  globalFileId: number;
  docHeaderId: number;
  documentCode: string;
  fileName: string;
  lineNo: number;
  remark: string;
  fileUploadLink: string;
  fileType: string;
  icon: string;
  formDescription?: string;
  callbackRefid: string;
  trxFileId: number;
}