import { Lov } from "../procurement/lov";

export interface WfTask {
  order: number,
  empCode: string,
  apprId?: number,
  trxId: string,
  seq: number;
  remarks: string;
  statusDate?: Date;
  status: Lov;
  role: Lov;
  apprDate?: Date;
  employee: WfEmployee;
  canEdit: Boolean,
  btnCtrl: {
    up: Boolean;
    down: Boolean;
    delete: Boolean;
  }
  createDate?: Date;
}
export interface WfEmployee {
  profileImg: string;
  code: string;
  title: string;
  firstname: string;
  lastname: string;
  positionId?: number;
  costCenterCode?: string;
  position?: string; 
}

export interface WfDialogData {
  projectCode: string,
  projectId: number,
  projectStepId: number,
  docHeaderId: number,
  createBy?: string;
}

export interface WfRequest {
  apprId: number;
  docHeaderId: number;
  projectCode: string;
  projectStepId: number;
  workflows: WfRequestItem[];
}

export interface WfRequestItem {
  apprRole: number;
  empCostCenter: string;
  empNo: string;
  empPositionId: number;
  remark: string;
  seqNo: number;
  trxApprId: number;
  empPosition: string;
}

export interface WorkflowBtnRequest {
  apprId: number,
  sequenceNo: number
  remark: string,
  forwardTo?: string,
  requestFrom?: string,
  additionalDoc?: boolean;
}

export interface EmitApproveBtnResult {
  result: boolean,
}
