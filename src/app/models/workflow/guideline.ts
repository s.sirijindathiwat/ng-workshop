import { DocumentStatus } from "../procurement/enum/document-status";

export interface Guideline {
    code: number, // projectStepId
    name: string, // step_name
    description: string, // STEP_DESC
    costCenterCode: string,
    costCenterName?: string,
    employeeNumber: string,
    employeeName?: string,
    btnEmployee: boolean,
    btnCostCenter?: boolean,
    docStatus: DocumentStatus,
}

export interface StepMaster {
    stepCode: string;
    stepName: string;
    stepDescription: string;
}