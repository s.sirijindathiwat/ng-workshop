import { Header } from "../common-response/header";

export interface ValidateButtonResponse {
    data: ValidateButton;
    header: Header;
}

export interface ValidateButton {
    status: boolean;
    errorMsg: string;
    additionShow: string;
}