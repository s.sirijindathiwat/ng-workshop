export interface CombineApproval {
    projectStepId: number;
    projectId?: any;
    projectCode: string;
    prNumber: string;
    poNumber?: any;
}