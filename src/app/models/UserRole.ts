export enum UserRole {
  Creator = 'CRE',
  Reporter = 'RPT',
  ProjectManager = 'PM',
  ProjectSponsor = 'PS',
  ITServiceCenter = 'ITS',
  ITPMO = 'PMO',
  ProjectOwnerManager = 'POM',
  PlanningAndBudgetingDepartment = 'PBD',
  FinancialDepartment = 'FID',
}
