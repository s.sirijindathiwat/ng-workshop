import { Paging } from "../paging";
import { Header } from "./header";

export class ResponseListPaging {
    header?: Header;
    data?: any[];
    paging? : Paging;
} 