export interface BudgetPopupElement {
  code: string;
  name: string;
}
