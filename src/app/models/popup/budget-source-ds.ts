export interface BudgetSourcePopupElement {
  code: string;
  name: string;
}
