export interface SupplierPopupElement {
  id: number;
  code: string;
  name: string;
}
export interface PrSearchPopupElement {
  prNumber: number;
  prDescription: string;
  erpPrNumber: string;
}
export interface PoSearchPopupElement {
  poNumber: number;
  poDescription: string;
  erpPoNumber: string;
}
export interface StandardPopupElement {
  code: string;
  name: string;
}

export interface SupplierPopupElement {
  id: number;
  code: string;
  name: string;
}

export interface ItemPopupElement {
  inventoryItemId: number
  itemCode: string
  itemDescription: string
  categoryId: number
  categoryCode: string
  categoryDesc: string
}
