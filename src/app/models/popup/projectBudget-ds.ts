export interface ProjectBudgetPopupElement {
  budgetCode: string;
  budgetName: string;
  budgetYear: string;
}
