export interface BudgetTypePopupElement {
  code: string;
  name: string;
}
