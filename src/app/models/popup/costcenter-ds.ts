export interface CostCenterPopupElement {
    code: string;
    name: string;
}
