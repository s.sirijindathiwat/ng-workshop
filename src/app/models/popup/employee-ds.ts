export interface EmployeePopupElement {
  employeeNumber: string;
  title: string;
  firstname: string;
  lastname: string;
  position?: string;
  positionId?: number;
  costCenterCode?: string;
}
