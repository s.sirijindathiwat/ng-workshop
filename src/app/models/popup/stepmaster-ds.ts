export interface StepMasterPopupElement {
    code: string;
    name: string;
}
