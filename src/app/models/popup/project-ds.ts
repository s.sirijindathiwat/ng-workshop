export interface ProjectPopupElement {
  code: string;
  name: string;
}
