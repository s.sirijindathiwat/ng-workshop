export interface AccountPopupElement {
  code: string;
  name: string;
}
