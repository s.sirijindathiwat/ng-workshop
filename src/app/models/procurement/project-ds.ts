export interface ProjectDs {
    index: number
    projectCode: string;
    projectName: string;
    projectMethodName: string;
    budgetYear: Date;
    projectCategoryName: string;
    projectBudgetAmt: number;
    costCenterName: string;
    statusName: string;
  }