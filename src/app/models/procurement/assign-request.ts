export interface AssignRequest {
    assignProjectItemList: AssignProjectItemList[];
    projectCode: string;
}

export interface AssignProjectItemList {
    agencyCode: string;
    code: number;
    employeeCode: string;
}
