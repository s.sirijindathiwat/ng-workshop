export interface GuidelineMenu {
  projectCode: string;
  navMenu: GuidelineMenuNav[]
}

export interface GuidelineMenuNav {
  link: string;
  menuName: string;
  projectStepId: number;
  status: string;
  projectStatus: string;
  recommendDesc: string;
  isApprovalRequired: boolean;
  combineApprovalType: string;
  child?: GuidelineMenuNav[];
}