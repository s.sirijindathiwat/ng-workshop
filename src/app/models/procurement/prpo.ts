export class Supplier {
  id?: number;
  code?: string;
  name?: string;
}
