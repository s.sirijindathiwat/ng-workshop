export interface  BoardElement {
    boardId: number
    index: number
    fileName: string
    path: string
    empCode: string;
    prefix: string;
    firstName: string;
    lastName: string;
    position: string;
    costCenterName: number;
    responsibility: string;
    callbackRefid: string;
    button: string;
}

export interface BoardElementInfo3 {
    id: number
    empCode: string
    prefix: string
    firstName: string
    lastName: string
    position: string
    costCenterName: string
    responsibilityCode: string
    fileName: string
    fileUploadLink: string
  }
