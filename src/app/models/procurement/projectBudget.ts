export class ProjectBudget {
  budgetCode?: string;
  budgetName?: string;
  budgetYear?: string;
  budgetAmount?: number;
}
