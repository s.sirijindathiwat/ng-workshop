export enum ProjectStatus {
    NEW = "NEW",
    OPEN = "OPEN",
    FREEZE = "FREEZE",
    CANCELLED = "CANCELLED",
    CANCEL = "CANCEL",
    COMPLETED = "COMPLETED"
  }
