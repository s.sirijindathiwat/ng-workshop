export enum PoStatus {
  ERROR = "ERROR",
  APPROVED = "APPROVED",
  ENTRY = "ENTRY",
  IN_PROCESS = "INPROCESS",
}
