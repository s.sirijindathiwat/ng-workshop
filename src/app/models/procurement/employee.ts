export interface Employee {
    employeeNumber?: string;
    title?: string;
    firstName?: string;
    lastName?: string;
    costCenter?: string;
    position?: string;
    positionId?: number;
}