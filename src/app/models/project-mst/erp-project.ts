import { Header } from "../common-response/header";
import { Paging } from "../paging";

export interface SearchErpProjectResponse {
    data: SearchErpProject[];
    header: Header;
    paging: Paging;
}
export interface SearchErpProject {
    description: string;
    name: string;
    projectId: number;
    segment1: string;
}

export interface SearchErpProjectPopupElement {
    projectId: number;
    segment1: string;
    name: string;
    description: string;
  }