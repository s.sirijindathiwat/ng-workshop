export class version {
  AppVersion!: string;
  BuildDate!: string;
  AppName!: string;
  CurrentProfile!: string;
}
