export interface FileData extends File {
  extentions: string;
  icon: string;
}

export interface faIcon {
  icon: string,
  mimeType: Array<string>
}

export const faFileIcon: faIcon[] = [
  {
    icon: "fa-file-excel",
    mimeType: [
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "application/vnd.ms-excel"
    ]
  },
  {
    icon: "fa-file-pdf",
    mimeType: [
      "application/pdf"
    ]
  },
  {
    icon: "fa-file-word",
    mimeType: [
      "application/msword",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ]
  },
  {
    icon: "fa-file-image",
    mimeType: [
      "image/jpeg",
      "image/gif",
      "image/png",
      "image/vnd.dwg"
    ]
  },
  {
    icon: "fa-file-archive",
    mimeType: [
      "application/zip",
      "application/vnd.rar",
      "application/x-tar",
      "application/x-7z-compressed",
      "application/x-rar-compressed",
    ]
  },
  {
    icon: "fa-file-alt",
    mimeType: []
  }
]

export interface FileDialogData {
  projectCode: string,
  projectId?: number,
  projectStepId?: number,
  docHeaderId?: number
  documentCode?: string
}

export interface FileDialogDataUploadLevel {
  projectCode: string,
  projectId?: number,
  projectStepId?: number,
  docHeaderId?: number,
  documentCode?: string,
  uploadLevel?: string,
}

