import { Injectable, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DatePipe } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
//Jwt Authen
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatRippleModule,
  NativeDateAdapter,
} from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { MatCarouselModule } from 'ng-mat-carousel';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MonacoEditorModule, MONACO_PATH } from '@materia-ui/ngx-monaco-editor';
import { MatTreeModule } from '@angular/material/tree';

import { HttpErrorInterceptor } from './interceptor/httpErrorInterceptor';
import { JwtInterceptor } from './interceptor/jwt.interceptor';
import { ToastrModule } from 'ngx-toastr';

import { LoaderComponent } from './components/loader/loader.component';
import { CardComponent } from './components/card/card.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuListComponent } from './components/menu-list/menu-list.component';

import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { ProjectSummaryComponent } from './components/project/project-summary/project-summary.component';
import { ProjectDetailComponent } from './components/project/project-detail/project-detail.component';
import { SearchProjectComponent } from './components/project/project-summary/search-project/search-project.component';
import { SearchPlanComponent } from './components/project/project-summary/search-plan/search-plan.component';
import { SearchAgencyComponent } from './components/project/project-detail/operator-manage/search-agency/search-agency.component';
import { SearchEmployeeComponent } from './components/project/project-detail/operator-manage/search-employee/search-employee.component';
import { SelectProjectCategoryComponent } from './components/project/project-detail/select-project-category/select-project-category.component';
import { OperatorManageComponent } from './components/project/project-detail/operator-manage/operator-manage.component';
import { DialogMasterComponent } from './components/dialog/dialog-master/dialog-master.component';
import { ProjectPopupComponent } from './components/procure-popup/project-popup/project-popup.component';
import { BudgetPopupComponent } from './components/procure-popup/budget-popup/budget-popup.component';
import { ErpProjectPopupComponent } from './components/procure-popup/erp-project-popup/erp-project-popup.component';
import { CostcenterPopupComponent } from './components/procure-popup/costcenter-popup/costcenter-popup.component';
import { ConfirmPopupComponent } from './components/procure-popup/confirm-popup/confirm-popup.component';

import { SharedModule } from './shared/shared.module';

@Injectable()
export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      let day: string = date.getDate().toString();
      day = +day < 10 ? '0' + day : day;
      let month: string = (date.getMonth() + 1).toString();
      month = +month < 10 ? '0' + month : month;
      let year = date.getFullYear();
      if (date.getFullYear() < 2500) year = date.getFullYear() + 543;
      else year = date.getFullYear();
      return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
    }
    return date.toDateString();
  }
  private _to2digit(n: string) {
    return ('00' + n).slice(-2);
  }
}

export const APP_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'input',
    monthYearLabel: 'input',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'DD/MMM/YYYY',
  },
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    LoaderComponent,
    CardComponent,
    HeaderComponent,
    MenuListComponent,
    ProjectSummaryComponent,
    ProjectDetailComponent,
    SearchProjectComponent,
    SearchPlanComponent,
    SearchAgencyComponent,
    SearchEmployeeComponent,
    SelectProjectCategoryComponent,
    ProjectSummaryComponent,
    OperatorManageComponent,
    DialogMasterComponent,
    ProjectPopupComponent,
    BudgetPopupComponent,
    ErpProjectPopupComponent,
    CostcenterPopupComponent,
    ConfirmPopupComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LayoutModule,
    FlexLayoutModule,
    FormsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    MatMenuModule,
    MatCardModule,
    MatExpansionModule,
    MatDialogModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatStepperModule,
    MatTableModule,
    MatTooltipModule,
    MatSortModule,
    ToastrModule.forRoot(),
    MatRippleModule,
    MatGridListModule,
    NgxDropzoneModule,
    MatCarouselModule.forRoot(),
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MonacoEditorModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatChipsModule,
    MatTreeModule,
    SharedModule,
  ],
  exports: [
    MatTabsModule,
    MatMenuModule,
    MatCardModule,
    MatButtonModule,
    MatExpansionModule,
    MatDialogModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    MatSortModule,
    MatSidenavModule,
    MatChipsModule,
    MatPaginatorModule,
    MatTreeModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_LOCALE, useValue: 'th-TH' },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
    {
      provide: MONACO_PATH,
      useValue: 'https://unpkg.com/monaco-editor@0.24.0/min/vs',
    },

    // provider used to create fake backend
    MatDatepickerModule,
    DatePipe,
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
