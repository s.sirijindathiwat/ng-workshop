import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from '../../services/login/auth.service'; 
import { utility_helper } from '../../helpers/utility_helper';   
import { version } from '../../models/version';

export class Myslides {
  image!: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  versionP!: version;
  versionA!: version;
  loginForm!: FormGroup;
  loading = false;
  submitted = false;
  returnUrl!: string;
  role !: string; 
  error = ''; 
  slides !: Array<Myslides>;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    public util: utility_helper, 

  ) {
    // redirect to home if already logged in
    if (this.authService.currentUserValue) {
      this.router.navigate(['/dashboard']);
    }
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = 'dashboard';
    this.ListAnnouncementData();

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  // tslint:disable-next-line:typedef
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    this.role = "MAKER";
    
    this.authService.login(this.loginForm?.controls['username'].value, this.loginForm?.controls['password'].value)
      .pipe(first())
      .subscribe(
        data => {
          if (data) {
            this.authService.getUserRole(this.loginForm?.controls['username'].value).subscribe(role => {
              this.role = role.data?.roleCode;
                this.authService.getUserDetailLogin(this.loginForm?.controls['username'].value, role.data?.roleCode).subscribe(userDetail => {
                  if (userDetail) {
                    this.router.navigate([this.returnUrl]);
                  }
                });
            });
            this.authService.getUserDetailLogin(this.loginForm?.controls['username'].value, this.role).subscribe(userDetail => {
              if (userDetail) {
                this.router.navigate([this.returnUrl]);
              }
            });
          }
          else {
            this.util.showErrortoastr("กรุณาตรวจสอบ username หรือ password ให้ถูกต้อง", "");
          }
        },
        error => {
          this.error = error.error;
          this.loading = false;
        });
  }

  ListAnnouncementData() {

    this.slides = new Array<Myslides>(); 
  }

  showversion() { 

    this.authService.serviceVersion().subscribe(data => {
      if (data != null) {
        this.versionA = data;
      }
    });

    this.util.showSuccesstoastr(this.versionP.AppName + " " + this.versionP.AppVersion + " " + this.versionP.BuildDate + " " + this.versionA.AppName + " " + this.versionA.AppVersion + " " + this.versionA.BuildDate + " " + this.versionA.CurrentProfile, "");
  }

  
}
