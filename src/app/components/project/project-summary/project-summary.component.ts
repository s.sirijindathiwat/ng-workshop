import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MenuList } from '../../../components/navbar-top-menu/navbar-top-menu.component';
import { ProjectService } from '../../../services/project.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Lov } from '../../../models/procurement/lov';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { BudgetPopupComponent } from '../../../components/procure-popup/budget-popup/budget-popup.component';
import { ProjectPopupComponent } from '../../../components/procure-popup/project-popup/project-popup.component';
import { MatSort } from '@angular/material/sort';
import { Paging } from '../../../models/paging';
import { AppDateAdapter } from '../../../shared/date-adapter';
import { APP_DATE_FORMATS } from '../../../directives/date.directive';
import { ProjectDs } from '../../../models/procurement/project-ds';
import { Project } from '../../../models/procurement/project';

const moment = _rollupMoment || _moment;
@Component({
  selector: 'app-project-summary',
  templateUrl: './project-summary.component.html',
  styleUrls: ['./project-summary.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
  ],
})
export class ProjectSummaryComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  @Input() rootMenu?: MenuList[];
  destroy$: Subject<boolean> = new Subject<boolean>();
  pageEvent?: PageEvent;
  pageSize: number = 5;
  totalAll: number = 5;

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  paging?: Paging;

  listMenu?: MenuList[];
  currentMenu: MenuList = {
    title: 'โครงการ/งาน',
    link: '',
    isActive: true,
  };

  state: any;

  displayedColumns: string[] = [
    'index',
    'projectCode',
    'projectName',
    'projectMethodName',
    'budgetYear',
    'projectCategoryName',
    'projectBudgetAmt',
    'costCenterName',
    'statusName',
  ];
  dataSource = new MatTableDataSource<ProjectDs>();

  date = new FormControl(new Date());
  projectCategoryList?: Lov[];

  searchform: FormGroup = new FormGroup({
    projectCode: new FormControl({ value: '', disabled: false }),
    projectName: new FormControl({ value: '', disabled: false }),
    projectBudgetCode: new FormControl({ value: '', disabled: false }),
    projectBudgetName: new FormControl({ value: '', disabled: false }),
    projectCategoryCode: new FormControl({ value: '', disabled: false }),
    budgetYear: this.date,
  });

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private projectService: ProjectService
  ) {
    if (this.rootMenu === undefined) {
      this.listMenu = [
        {
          title: 'กระบวนการจัดหาพัสดุ',
          link: '',
          isActive: false,
        },
      ];
    }
    this.listMenu?.push(this.currentMenu);

    this.projectService
      .getProjectCategoryList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.projectCategoryList = _res));
  }
  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
  ngAfterViewInit(): void {
    console.log('== ngAfterViewInit ==');
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    console.log('==ngOnInit== history: ' + JSON.stringify(history.state));
    if (history.state.searchParams !== undefined) {
      console.log('==ngOnInit== SET ==');
      this.searchform.setValue({
        projectCode: history.state.searchParams.projectCode,
        projectBudgetCode: history.state.searchParams.projectBudgetCode,
        projectCategoryCode: history.state.searchParams.projectCategoryCode,
        budgetYear: new Date(),
      });
      this.onSearch();
    }
  }

  chooseYearHandler(normalizedYear: Date, datepicker: MatDatepicker<any>) {
    this.date.setValue(normalizedYear);
    this.searchform.get('budgetYear')?.setValue(normalizedYear);
    console.log(normalizedYear);
    datepicker.close();
  }

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  selectProject() {
    const dialogRef = this.dialog.open(ProjectPopupComponent, {
      width: '750px',
      minWidth: 'calc(100vh - 90px)',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.length > 0) {
          this.searchform.get('projectCode')?.setValue(result[0].code);
          this.searchform
            .get('projectName')
            ?.setValue(result[0].code + ': ' + result[0].name);
        } else {
          this.searchform.get('projectCode')?.setValue('');
          this.searchform.get('projectName')?.setValue('');
        }
      } else {
        this.searchform.get('projectCode')?.setValue('');
      }
    });
  }

  selectProjectBudget() {
    const dialogRef = this.dialog.open(BudgetPopupComponent, {
      width: '750px',
      minWidth: 'calc(100vh - 90px)',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.length > 0) {
          this.searchform
            .get('projectBudgetCode')
            ?.setValue(result[0].budgetCode);
          this.searchform
            .get('projectBudgetName')
            ?.setValue(result[0].budgetCode);
        } else {
          this.searchform.get('projectBudgetCode')?.setValue('');
          this.searchform.get('projectBudgetName')?.setValue('');
        }
      }
    });
  }
  onSearch() {
    console.log(this.searchform.value);
    if (
      this.searchform.value.budgetYear !== null &&
      (this.searchform.value.projectCode === '' ||
        this.searchform.value.projectCategoryCode === '')
    ) {
      if (
        (this.searchform.value.budgetYear !== null &&
          this.searchform.value.projectCategoryCode !== '') ||
        (this.searchform.value.budgetYear !== null &&
          this.searchform.value.projectBudgetCode !== '')
      ) {
      } else if (
        !(
          this.searchform.value.budgetYear !== null &&
          this.searchform.value.projectCode !== ''
        )
      ) {
        this._snackBar.open(
          'กรุณาเลือก Field ค้นหาอย่างน้อย 2 Field',
          'close',
          {
            panelClass: ['snackbar-error'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
            duration: 5000,
          }
        );
        return;
      }
    }
    this.projectService
      .searchProject(
        this.searchform.value,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe(
        (resp: { data: Project[]; paging: Paging }) => {
          var data: Project[] = resp.data;
          var uniq: any = [...new Set(data)].map(
            (_item: any, index: number) => {
              _item.index =
                index + 1 + this.paginator.pageSize * this.paginator.pageIndex;
              return _item;
            }
          );
          this.dataSource = new MatTableDataSource<ProjectDs>(uniq);
          let budgetYear: Moment = this.searchform.get('budgetYear')?.value;
          this.state = {
            searchParams: {
              projectCode: this.searchform.get('projectCode')?.value,
              projectBudgetCode:
                this.searchform.get('projectBudgetCode')?.value,
              projectCategoryCode: this.searchform.get('projectCategoryCode')
                ?.value,
              budgetYear: budgetYear.toISOString(),
            },
          };

          this.paging = resp.paging;
          this.totalAll = this.paging?.totalAll!;
          this.paginator.length = this.paging?.totalAll!;
          this.paginator.pageSize = this.pageSize;
        },
        (err) => {
          this._snackBar.open(err, 'close', {
            panelClass: ['snackbar-error'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
            duration: 5000,
          });
        }
      );
  }

  onEdit(projectCode: string) {
    console.log('onEdit' + projectCode);
    const navigationDetails: string[] = ['/projects'];
    if (projectCode.length) {
      navigationDetails.push(projectCode);
    }
    this.router.navigate(navigationDetails, { state: this.state });
  }

  onDelete(projectCode: string) {
    console.log(`ondelete : ${projectCode}`);
    this.projectService.deleteProject(projectCode).subscribe((_res) => {
      this._snackBar.open(_res.data, 'close', {
        panelClass: ['snackbar-success'],
        horizontalPosition: 'right',
        verticalPosition: 'top',
      });
      this.onSearch();
    });
  }

  public paginatorEvent(e: PageEvent): PageEvent | any {
    console.log(e);
    this.pageSize = e.pageSize;
    this.onSearch();
    return e;
  }

  onCreateProject() {
    this.router.navigate(['/projects/create'], { state: this.state });
  }

  clear() {
    for (let formname in this.searchform.controls) {
      this.searchform.controls[formname].setValue('');
    }
    this.searchform.controls['budgetYear'].setValue(new Date());
  }
  clearDatepicker(formControlName: string) {
    this.searchform.controls[formControlName].setValue(null);
  }
}
