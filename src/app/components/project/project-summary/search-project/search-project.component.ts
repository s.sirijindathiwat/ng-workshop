import {Component, OnInit,} from '@angular/core';
import { MatDialogRef} from "@angular/material/dialog"; 
import {SelectionModel} from "@angular/cdk/collections";
import {MatTableDataSource} from "@angular/material/table"; 
export interface PeriodicElement {
  projectName: string;
  projectCode: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},
  {projectCode: 'C003-108528', projectName: 'พัฒนาระบบงาน sub-system'},

];

@Component({
  selector: 'app-search-project',
  templateUrl: './search-project.component.html',
  styleUrls: ['./search-project.component.css']
})


export class SearchProjectComponent implements OnInit {
  // displayedColumns: string[] = ['select', 'projectCode', 'projectName'];
  displayedColumns: string[] = ['select', 'projectCode', 'projectName'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  // }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  constructor(
    private dialogRef: MatDialogRef<SearchProjectComponent>,
    ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    console.log("== ngOnInit ==");
  }

  search() {

  }
}
