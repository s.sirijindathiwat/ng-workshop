import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { SelectionModel } from "@angular/cdk/collections";
import { MatDialogRef } from "@angular/material/dialog";
export interface PeriodicElement {
  planCode: string;
  planName: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },
  { planCode: 'C003-108528', planName: 'พัฒนาระบบงาน sub-system' },

];

@Component({
  selector: 'app-search-plan',
  templateUrl: './search-plan.component.html',
  styleUrls: ['./search-plan.component.css']
})
export class SearchPlanComponent implements OnInit {
  displayedColumns: string[] = ['select', 'planCode', 'planName'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  constructor(
    private dialogRef: MatDialogRef<SearchPlanComponent>,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void { console.log("== ngOnInit =="); }

  search() { }
}
