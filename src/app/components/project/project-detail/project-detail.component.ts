import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogMasterComponent } from '../../../components/dialog/dialog-master/dialog-master.component';
import { MenuList } from '../../../components/navbar-top-menu/navbar-top-menu.component';
import { BudgetPopupComponent } from '../../../components/procure-popup/budget-popup/budget-popup.component';
import { ConfirmPopupComponent } from '../../../components/procure-popup/confirm-popup/confirm-popup.component';
import { CostcenterPopupComponent } from '../../../components/procure-popup/costcenter-popup/costcenter-popup.component';
import { ErpProjectPopupComponent } from '../../../components/procure-popup/erp-project-popup/erp-project-popup.component';
import { OperatorManageComponent } from '../../../components/project/project-detail/operator-manage/operator-manage.component';
import { SelectProjectCategoryComponent } from '../../../components/project/project-detail/select-project-category/select-project-category.component';
import { APP_DATE_FORMATS } from '../../../directives/date.directive';
import { FileData, FileDialogData } from '../../../models/file/file';
import { Paging } from '../../../models/paging';
import { CostCenterPopupElement } from '../../../models/popup/costcenter-ds';
import {
  AssignProjectItemList,
  AssignRequest,
} from '../../../models/procurement/assign-request';
import { ProjectStatus } from '../../../models/procurement/enum/project-status';
import { Lov } from '../../../models/procurement/lov';
import { Project } from '../../../models/procurement/project';
import { SearchErpProjectPopupElement } from '../../../models/project-mst/erp-project';
import { Guideline } from '../../../models/workflow/guideline';
import { ProjectService } from '../../../services/project.service';
import { AppDateAdapter } from '../../../shared/date-adapter';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Subject, Subscription } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
const moment = _rollupMoment || _moment;

enum PageState {
  Create,
  Freeze,
  Update,
}

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
  ],
})
export class ProjectDetailComponent implements OnInit, OnDestroy {
  // OnDestroy
  destroy$: Subject<boolean> = new Subject<boolean>();

  @Input() rootMenu?: MenuList[];

  pageState: PageState = PageState.Create;

  listMenu?: MenuList[];
  currentMenu: MenuList = {
    title: 'โครงการ/งาน',
    link: '',
    isActive: true,
  };
  // date = new FormControl(new Date());
  form: FormGroup = new FormGroup({
    projectBudgetCode: new FormControl({ value: null, disabled: false }),
    projectBudgetName: new FormControl({ value: null, disabled: true }),
    projectCode: new FormControl({ value: 'NEW', disabled: true }),
    projectName: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    projectCategoryCode: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    projectTypeCode: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    erpProjectCode: new FormControl({ value: '', disabled: false }),
    erpProjectDescr: new FormControl({ value: '', disabled: false }),
    budgetYear: new FormControl(
      { value: null, disabled: false },
      Validators.required
    ),
    projectBudgetAmt: new FormControl(
      { value: null, disabled: false },
      Validators.required
    ),

    currencyCode: new FormControl({ value: null, disabled: true }),
    exchangeDate: new FormControl({ value: null, disabled: true }),
    exchangeRate: new FormControl({ value: null, disabled: true }),

    projectCharacterCode: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    projectCharacterTypeCode: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    purchaseTypeCode: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    projectMethodCode: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    conditionCode: new FormControl({ value: '', disabled: false }),

    agreementTypeCode: new FormControl(
      { value: '', disabled: false },
      Validators.required
    ),
    costCenterCode: new FormControl({ value: '', disabled: false }),
    costCenterName: new FormControl({ value: null, disabled: false }),
    procureToPay: new FormControl(
      { value: true, disabled: false },
      Validators.required
    ),
    projectStatus: new FormControl({ value: 'NEW', disabled: true }),
    functionalCurrency: new FormControl({ value: false, disabled: false }),
  });

  formFunctionalCurrency: FormGroup = new FormGroup({
    currencyCode: new FormControl(
      { value: null, disabled: false },
      Validators.required
    ),
    exchangeDate: new FormControl({ value: null, disabled: false }),
    exchangeRate: new FormControl(
      { value: null, disabled: false },
      Validators.required
    ),
  });

  tmpList: any;
  isProcure: any;
  //  =  [
  //   { code: true, name: 'Yes' },
  //   { code: false, name: 'No' }
  // ];

  dimMethodCondition: boolean = false;
  btnSaveCtrl: boolean = true;
  btnFreezeCtrl: boolean = false;
  bntAssignCtrl: boolean = false;
  switchFreeze: boolean = true;
  isDimStatusCancelled: boolean = false;

  budgetAmount: any;

  projectCategoryList: any;
  projectTypeList: any;
  projectCharacterList: any;
  projectCharacterTypeList?: Lov[];
  projectStatus: ProjectStatus | undefined;

  projectMethodList?: Lov[];
  projectMethodConditionList?: Lov[];
  purchaseTypeList: any;

  currenciesList: any;
  erpProjectsList: any;
  agreementTypeList: any;
  accountSegmentsList: any;

  loading = false;
  timeOut?: number;

  private projectCharacterSubscription?: Subscription;
  private projectMethodListSubscription?: Subscription;
  private projectMethodConditionListSubscription?: Subscription;
  private projectMasterByIdSubscription?: Subscription;

  projectCode: string = '';
  project: string = '';

  // upload/download
  files: FileData[] = [];
  btnSaveLoading: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private projectService: ProjectService,
    public toastrService: ToastrService
  ) {
    console.log('==constructor==');
    if (this.rootMenu === undefined) {
      this.listMenu = [
        {
          title: 'กระบวนการจัดหาพัสดุ',
          link: '',
          isActive: false,
        },
      ];
    }
    this.listMenu?.push(this.currentMenu);

    this._initializeData();

    this.activatedRoute.params.subscribe((params) => {
      console.log(params['id']);
      if (params['id'] !== undefined) {
        this.pageState = PageState.Update;
        this.projectCode = params['id'];
        this.btnFreezeCtrl = true;
      }
    });
  }

  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnInit(): void {
    console.log('==ngOnInit==');
    if (this.pageState === PageState.Update) {
      this._loadProjectByProjectCode(this.projectCode);
    }
    console.log('==ngOnInit== history: ' + JSON.stringify(history.state));
  }

  private _loadProjectByProjectCode(projectCode: string) {
    this.projectMasterByIdSubscription = this.projectService
      .getProjectMasterById(projectCode)
      .subscribe((_res: Project) => {
        this.projectStatus = _res.projectStatus;
        this.projectMethodListSubscription = this.projectService
          .getProjectMethodListByCategoryCode(_res.projectCategoryCode)
          .subscribe((_resp) => (this.projectMethodList = _resp));
        this.projectMethodConditionListSubscription = this.projectService
          .getProjectMethodConditionByProjectMethodCode(_res.projectMethodCode)
          .subscribe((_resp) => {
            if (_resp.length > 0) {
              this.projectMethodConditionList = _resp;
              this.dimMethodCondition = false;
            } else {
              this.projectMethodConditionList = [];
              this.dimMethodCondition = true;
            }
          });
        this.projectCharacterSubscription = this.projectService
          .getProjectCharacterTypeList(_res.projectCharacterCode)
          .subscribe((_resp) => (this.projectCharacterTypeList = _resp));
        this.form.patchValue(_res);
        if (
          _res.projectStatus === ProjectStatus.CANCELLED ||
          _res.projectStatus === ProjectStatus.CANCEL ||
          _res.projectStatus === ProjectStatus.FREEZE
        ) {
          this.isDimStatusCancelled = true;
          this.form.disable();
        }
        if (_res.projectBudgetCode != null) {
          this.projectService
            .searchProjectBudgetByCodeAndName(_res.projectBudgetCode, 0, 1)
            .subscribe((resp: { data: any; paging: Paging }) => {
              console.log(resp.data);
              this.form
                .get('projectBudgetName')
                ?.setValue(resp.data[0].budgetName);
            });
        }

        if (_res.erpProjectCode != null) {
          this.form.get('erpProjectCode')?.setValue(_res.erpProjectCode);
          this.form.get('erpProjectDescr')?.setValue(_res.erpProjectDescr);
        }

        if (_res.functionalCurrency) {
          this.form.get('currencyCode')?.enable();
          this.form.get('exchangeDate')?.enable();
          this.form.get('exchangeRate')?.enable();

          this.form.controls['currencyCode'].setValidators([
            Validators.required,
          ]);
          this.form.controls['exchangeRate'].setValidators([
            Validators.required,
          ]);

          for (var formName in this.form.controls) {
            this.form.controls[formName].updateValueAndValidity();
          }
        }
        this.form
          .get('projectBudgetAmt')
          ?.setValue(this.form.value.projectBudgetAmt);
        console.log(`_res.projectStatus: ${_res.projectStatus}`);
        console.log(
          `_res.projectStatus: ${_res.projectStatus === ProjectStatus.FREEZE} `
        );

        if (_res.projectStatus === ProjectStatus.FREEZE) {
          this._ctrlBtn(true);
        } else if (
          _res.projectStatus === ProjectStatus.CANCELLED ||
          _res.projectStatus === ProjectStatus.CANCEL
        ) {
          this._ctrlBtn(true, true);
        }
        console.log(JSON.stringify(_res));
      });
  }

  private _initializeData() {
    this.projectService
      .getProjectCategoryList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.projectCategoryList = _res));
    this.projectService
      .getProjectTypeList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.projectTypeList = _res));
    this.projectService
      .getProjectCharacterList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.projectCharacterList = _res));
    this.projectService
      .getPurchaseTypeList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.purchaseTypeList = _res));
    this.projectService
      .getCurrenciesList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.currenciesList = _res));
    this.projectService
      .getErpProjectsList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.erpProjectsList = _res));
    this.projectService
      .getAgreementTypeList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.agreementTypeList = _res));
    this.projectService
      .getAccountSegmentsList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.accountSegmentsList = _res));
    this.projectService
      .getIsProcure()
      .pipe(takeUntil(this.destroy$))
      .subscribe((_res: Lov[]) => (this.isProcure = _res));
  }
  selectProjectMethodCondtion(event: any) {
    this.projectMethodConditionListSubscription?.unsubscribe();
    this.projectMethodConditionListSubscription = this.projectService
      .getProjectMethodConditionByProjectMethodCode(event.value)
      .subscribe((_res: Lov[]) => {
        if (_res.length > 0) {
          this.projectMethodConditionList = _res;
          this.dimMethodCondition = false;
        } else {
          this.projectMethodConditionList = [];
          this.dimMethodCondition = true;
        }
      });
  }
  checkButtonCtrl(isFreeze: boolean) {
    if (isFreeze) {
      this.bntAssignCtrl = true;
      this.form.disable();
    }
  }

  manageOperator() {
    const dialogRef = this.dialog.open(OperatorManageComponent, {
      height: '80%',
      width: '90%',
      data: {
        projectCode: this.form.get('projectCode')?.value,
        accountSegmentsList: this.accountSegmentsList,
      },
    });

    dialogRef.afterClosed().subscribe((result: Guideline[]) => {
      console.log('The dialog was closed : ', result);
      if (result != null) {
        let resultfFilter = result.filter(
          (item) => item.costCenterCode != undefined
        );
        let assignProjectItemList: AssignProjectItemList[] = resultfFilter.map(
          (item: Guideline) => {
            let assignItem: AssignProjectItemList = {
              code: item.code,
              agencyCode: item.costCenterCode,
              employeeCode: item.employeeNumber,
            };
            return assignItem;
          }
        );
        let request: AssignRequest = {
          projectCode: this.projectCode,
          assignProjectItemList,
        };
        this.projectService
          .assignProjectAdministrator(request)
          .pipe(take(1))
          .subscribe((_res: any) => console.log(_res));
      }
    });
  }

  selectProjectCategory() {
    const dialogRef = this.dialog.open(SelectProjectCategoryComponent, {
      height: '600px',
      width: '600px',
      data: {
        currentValue: this.form.get('projectCategoryCode')?.value,
      },
    });

    dialogRef.afterClosed().subscribe((projectCategoryCode) => {
      this.form.get('projectCategoryCode')?.setValue(projectCategoryCode);
      this.projectMethodListSubscription?.unsubscribe();
      this.projectMethodListSubscription = this.projectService
        .getProjectMethodListByCategoryCode(projectCategoryCode)
        .subscribe((_res) => (this.projectMethodList = _res));
    });
  }

  selectProjectPlan() {
    const dialogRef = this.dialog.open(BudgetPopupComponent, {
      width: '550px',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.length > 0) {
          console.log('01/01/' + result[0].budgetYear);
          console.log(new Date('01/01/' + result[0].budgetYear));
          console.log(result[0]);
          console.log('The dialog was closed');
          this.form
            .get('budgetYear')
            ?.setValue(new Date('01/01/' + result[0].budgetYear));
          this.form.get('projectBudgetCode')?.setValue(result[0].budgetCode);
          this.form.get('projectBudgetName')?.setValue(result[0].budgetName);
          this.form.get('projectBudgetAmt')?.setValue(result[0].budgetAmount);
        } else {
          this.form.get('budgetYear')?.setValue(null);
          this.form.get('projectBudgetCode')?.setValue(null);
          this.form.get('projectBudgetName')?.setValue(null);
          this.form.get('projectBudgetAmt')?.setValue(null);
        }
      } else {
        this.form.get('budgetYear')?.setValue(null);
        this.form.get('projectBudgetCode')?.setValue(null);
        this.form.get('projectBudgetName')?.setValue(null);
        this.form.get('projectBudgetAmt')?.setValue(null);
      }
    });
  }

  selectProjectCharacter(event: any) {
    this.form.get('projectCharacterTypeCode')?.setValue('');
    this.projectCharacterSubscription?.unsubscribe();
    this.projectCharacterSubscription = this.projectService
      .getProjectCharacterTypeList(event.value)
      .subscribe((_res) => (this.projectCharacterTypeList = _res));
  }

  saveAndUpdate() {
    console.log(this.form.value);
    this.loading = true;
    this.btnSaveLoading = true;

    this.form.markAllAsTouched();
    if (this.form.invalid) {
      this.loading = false;
      this._snackBar.open('กรุณาตรวจสอบข้อมูลที่จำเป็น', 'close', {
        panelClass: ['snackbar-error'],
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 5000,
      });
      this.btnSaveLoading = false;
      return;
    }
    if (
      this.form.value.projectBudgetAmt > 100000 &&
      (this.form.value.projectBudgetCode == undefined ||
        this.form.value.projectBudgetCode == null)
    ) {
      this._snackBar.open(
        'กรณีวงเงินการจัดหาพัสดุมากกว่า 100,000 บาท ต้องจัดทำแผนการจัดซื้อจัดจ้างก่อนดำเนินการจัดหาพัสดุ ดำเนินการได้โดยเลือกเมนู แผนการจัดซื้อจัดจ้าง',
        'close',
        {
          panelClass: ['snackbar-error'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
          duration: 5000,
        }
      );
      this.btnSaveLoading = false;
      return;
    }

    if (this.pageState === PageState.Update) {
      this.btnSaveLoading = true;
      this.btnSaveCtrl = false;
      this.projectService.updateProject(this.form.getRawValue()).subscribe(
        (result: any) => {
          this._snackBar.open('ปรับปรุงข้อมูลสำเร็จ', 'close', {
            panelClass: ['snackbar-success'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
          this.form.get('projectCode')?.setValue(result.projectCode);
          this.form.get('projectStatus')?.setValue(result.projectStatus);
          this.projectStatus = result.projectStatus;
          this._ctrlBtn(false);
          this.form.markAsPristine();
          this.btnSaveLoading = false;
        },
        (_err) => {
          this._snackBar.open('ปรับปรุงข้อมูลไม่สำเร็จ\n' + _err, 'close', {
            panelClass: ['snackbar-error'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
            duration: 5000,
          });
          this.btnSaveCtrl = true;
          this.btnSaveLoading = false;
        }
      );
    } else {
      this.btnSaveLoading = true;
      this.btnSaveCtrl = false;
      this.projectService.saveProject(this.form.getRawValue()).subscribe(
        (result: any) => {
          this._snackBar.open('บันทึกข้อมูลสำเร็จ', 'close', {
            panelClass: ['snackbar-success'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
          this.form.get('projectCode')?.setValue(result.projectCode);
          this.form.get('projectStatus')?.setValue(result.projectStatus);
          this.projectStatus = result.projectStatus;
          this._ctrlBtn(false);
          this.form.markAsPristine();
          this.pageState = PageState.Update;
          this.btnSaveLoading = false;
        },
        (err) => {
          this.btnSaveCtrl = true;
          this.btnSaveLoading = false;
        }
      );
    }
    this.form
      .get('projectBudgetAmt')
      ?.setValue(parseFloat(this.form.value.projectBudgetAmt).toFixed(2));
  }

  onClickCurrency(event: any) {
    if (event.checked) {
      this.form.get('currencyCode')?.enable();
      this.form.get('exchangeDate')?.enable();
      this.form.get('exchangeRate')?.enable();
      this.form.controls['currencyCode'].setValidators([Validators.required]);
      this.form.controls['exchangeRate'].setValidators([Validators.required]);

      for (var formName in this.form.controls) {
        this.form.controls[formName].updateValueAndValidity();
      }
    } else {
      this.form.get('currencyCode')?.setValue(null);
      this.form.get('exchangeDate')?.setValue(null);
      this.form.get('exchangeRate')?.setValue(null);

      this.form.get('currencyCode')?.disable();
      this.form.get('exchangeDate')?.disable();
      this.form.get('exchangeRate')?.disable();

      this.form.get('currencyCode')?.clearValidators();
      this.form.get('exchangeRate')?.clearValidators();
      this.form.updateValueAndValidity();
    }
  }

  onFreeze(isFreeze: boolean) {
    if (this.form.dirty) {
      // AlertDirtyDialog
      const dialogRef = this.dialog.open(ConfirmPopupComponent, {
        data: {
          dialogTitle: 'คำเตือน มีการเปลี่ยนแปลงข้อมูล โดยยังไม่ทำการบันทึก',
          title: 'คุณต้องการบันทึกใช่หรือไม่?',
          btnTrueDescription: 'ใช่',
          btnFalseDescription: 'ไม่ใช่',
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        console.log(`Dialog result: ${result}`);
        if (result === undefined || result === false) {
          console.log(`== return ==`);
          return;
        }
        console.log(`== next and save ==`);
        // Call Service
        this._callFreeze(this.form.get('projectCode')?.value, isFreeze);
      });
    } else {
      this._callFreeze(this.form.get('projectCode')?.value, isFreeze);
    }
  }

  onCancel() {
    const dialogRef = this.dialog.open(DialogMasterComponent, {
      width: '300px',
      data: {
        header: 'แจ้งเตือน',
        detail: 'ต้องการยกเลิกเอกสารหรือไม่',
        button1: 'ตกลง',
        button2: 'ยกเลิก',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        console.log('onCancel');
        this.projectService
          .cancelProject(this.form.get('projectCode')?.value)
          .subscribe(
            (result: any) => {
              this._snackBar.open('ปรับปรุงข้อมูลสำเร็จ', 'close', {
                panelClass: ['snackbar-success'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
              this.form.get('projectCode')?.setValue(result.projectCode);
              this.form.get('projectStatus')?.setValue(result.projectStatus);
              this.form.markAsPristine();
              this._ctrlBtn(true, true);
              console.log('result', result);

              if (
                result.projectStatus === ProjectStatus.CANCELLED ||
                result.projectStatus === ProjectStatus.CANCEL
              ) {
                this.isDimStatusCancelled = true;
                this.form.disable();
              }
            },
            (_err) => {
              if (_err == 'Error: Data not found') {
                this._snackBar.open(
                  'ไม่สามารถยกเลิก Project ได้ เนื่องจากมีการจัดทำ PR หรือ PO เรียบร้อยแล้ว กรุณายกเลิก PR หรือ PO',
                  'close',
                  {
                    panelClass: ['snackbar-error'],
                    horizontalPosition: 'right',
                    verticalPosition: 'top',
                    duration: 5000,
                  }
                );
                return false;
              } else {
                this._snackBar.open(_err, 'close', {
                  panelClass: ['snackbar-error'],
                  horizontalPosition: 'right',
                  verticalPosition: 'top',
                  duration: 5000,
                });
                return false;
              }
            }
          );
      }
    });
  }

  onBack() {
    console.log('onBack');
    this.router.navigate(['projects'], {
      state: { searchParams: history.state.searchParams },
    });
  }

  chooseYearHandler(normalizedYear: Date, datepicker: MatDatepicker<any>) {
    // this.date.setValue(normalizedYear);
    this.form.get('budgetYear')?.setValue(normalizedYear);
    console.log(normalizedYear);
    datepicker.close();
  }

  private _callFreeze(projectCode: string, isFreeze: boolean) {
    this.projectService.freezeProject(projectCode).subscribe(
      (result: any) => {
        this._snackBar.open('ปรับปรุงข้อมูลสำเร็จ', 'close', {
          panelClass: ['snackbar-success'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
        this.form.get('projectCode')?.setValue(result.projectCode);
        this.form.get('projectStatus')?.setValue(result.projectStatus);
        this.form.markAsPristine();
        this._ctrlBtn(isFreeze);
      },
      (_err) => {
        this._snackBar.open('ปรับปรุงข้อมูลไม่สำเร็จ\n' + _err, 'close', {
          panelClass: ['snackbar-error'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
        return false;
      }
    );
  }

  private _ctrlBtn(isFreeze: boolean, isCancel: boolean = false) {
    if (isCancel) {
      this.btnFreezeCtrl = false;
      this.switchFreeze = true;
      this.btnSaveCtrl = false;
      this.bntAssignCtrl = false;
    } else {
      this.btnFreezeCtrl = !isFreeze;
      this.switchFreeze = !isFreeze;
      this.btnSaveCtrl = this.switchFreeze;
      this.bntAssignCtrl = isFreeze;

      if (this.switchFreeze) {
        this.form.enable();
        if (this.form.get('functionalCurrency')?.value === false) {
          this.form.get('currencyCode')?.disable();
          this.form.get('exchangeDate')?.disable();
          this.form.get('exchangeRate')?.disable();
        }
        this.form.get('projectCode')?.disable();
        this.form.get('projectStatus')?.disable();
      } else {
        this.form.disable();
      }
    }
  }

  selectErpProject() {
    const dialogRef = this.dialog.open(ErpProjectPopupComponent, {
      width: '550px',
      data: {},
    });

    dialogRef
      .afterClosed()
      .subscribe((result: SearchErpProjectPopupElement[]) => {
        if (result && result.length > 0) {
          this.form.get('erpProjectCode')?.setValue(result[0].projectId);
          this.form
            .get('erpProjectDescr')
            ?.setValue(result[0].projectId + ': ' + result[0].description);
        } else {
          this.form.get('erpProjectCode')?.setValue(null);
          this.form.get('erpProjectDescr')?.setValue(null);
        }
      });
  }

  projectCharacterTypeDisabled(): boolean {
    if (
      this.projectStatus === ProjectStatus.CANCELLED ||
      this.projectStatus === ProjectStatus.CANCEL ||
      this.projectStatus === ProjectStatus.FREEZE
    ) {
      return true;
    }
    if (this.projectCharacterTypeList?.length === 0) return true;
    return false;
  }

  projectMethodDisabled(): boolean {
    if (
      this.projectStatus === ProjectStatus.CANCELLED ||
      this.projectStatus === ProjectStatus.CANCEL ||
      this.projectStatus === ProjectStatus.FREEZE
    ) {
      return true;
    }
    if (this.projectMethodList?.length === 0) return true;
    return false;
  }

  onFindCurrentByCostCenter() {
    this.dialog
      .open(CostcenterPopupComponent, {
        width: '750px',
        minWidth: 'calc(100vh - 90px)',
        data: {},
      })
      .afterClosed()
      .subscribe(
        (_result: CostCenterPopupElement[]) => {
          console.log(_result);
          if (_result) {
            if (_result.length > 0) {
              this.form.controls['costCenterCode'].setValue(_result[0].code);
              this.form.controls['costCenterName'].setValue(
                _result[0].code + ': ' + _result[0].name
              );
            } else {
              this.form.controls['costCenterCode'].setValue(null);
              this.form.controls['costCenterName'].setValue('');
            }
          }
        },
        (_err) => {
          this._snackBar.open(_err, 'close', {
            panelClass: ['snackbar-error'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
            duration: 5000,
          });
        }
      );
  }
  clearDatepicker(formControlName: string) {
    this.form.controls[formControlName].setValue(null);
  }
}
