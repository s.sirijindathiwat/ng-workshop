import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Employee } from '../../../../../models/procurement/employee';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Subject } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-employee',
  templateUrl: './search-employee.component.html',
  styleUrls: ['./search-employee.component.css'],
})
export class SearchEmployeeComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  destroy$: Subject<boolean> = new Subject<boolean>();

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource: MatTableDataSource<Employee> = new MatTableDataSource<Employee>();
  selection = new SelectionModel<Employee>(false, []);

  displayedColumns: string[] = [
    'select',
    'code',
    'prefix',
    'firstName',
    'lastName',
  ];

  empCode = new FormControl('');
  empName = new FormControl('');

  constructor(
    private dialogRef: MatDialogRef<SearchEmployeeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dataSource = new MatTableDataSource<Employee>(this.data.employeeList);
    console.log(this.data.employeeList);
  }

  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    console.log('== ngOnInit ==');
  }

  search() {
    let filterList: Employee[] = [];
    if (this.empCode.value != '') {
      let filterData = this.data.employeeList.filter((item: Employee) =>
        item.employeeNumber?.toString().includes(this.empCode.value)
      );
      filterList.push(...filterData);
    }
    if (this.empName.value != '') {
      let filterData = this.data.employeeList.filter((item: Employee) =>
        item.firstName?.includes(this.empName.value)
      );
      filterList.push(...filterData);
    }
    if (this.empName.value == '' && this.empCode.value == '') {
      this.dataSource.data = this.data.employeeList;
      return;
    }
    this.dataSource.data = filterList;
    console.log(filterList);
    console.log(this.selection);
  }
}
