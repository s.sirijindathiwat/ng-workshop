import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { Lov } from '../../../../../models/procurement/lov';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-search-agency',
  templateUrl: './search-agency.component.html',
  styleUrls: ['./search-agency.component.css'],
})
export class SearchAgencyComponent implements OnInit, OnDestroy, AfterViewInit {
  destroy$: Subject<boolean> = new Subject<boolean>();
  displayedColumns: string[] = ['select', 'code', 'name'];
  selection = new SelectionModel<Lov>(false, []);

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  dataSource: MatTableDataSource<Lov> = new MatTableDataSource<Lov>();

  searchQuery = new FormControl('', [Validators.required]);

  constructor(
    private dialogRef: MatDialogRef<SearchAgencyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dataSource = new MatTableDataSource<Lov>(
      this.data.accountSegmentsList.slice(0, 100)
    );
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnInit(): void {
    console.log('== ngOnInit ==');
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  checkAllToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  search() {
    console.log(this.searchQuery);
    if (this.searchQuery.hasError('required')) {
      return 'กรุณาระบุข้อมูล';
    } else if (this.searchQuery.hasError('minlength')) {
      let _err = this.searchQuery.getError('minlength');
      let _requiredLength = _err.requiredLength;
      let _actualLength = _err.actualLength;
      return `กรุณาระบุข้อมูลไม่น้อยกว่า ${_requiredLength} ตัวอักษร (ปัจจุบันคุณมี ${_actualLength} ตัวอักษร)`;
    } else if (this.searchQuery.valid) {
      let filterData: Lov[] = this.data.accountSegmentsList.filter(
        (item: Lov) => item.name?.includes(this.searchQuery.value)
      );
      if (filterData.length == 0) {
        filterData = this.data.accountSegmentsList.filter((item: Lov) =>
          item.code?.includes(this.searchQuery.value)
        );
      }
      console.log(filterData);
      this.dataSource.data = filterData;
    }
    return '';
  }
}
