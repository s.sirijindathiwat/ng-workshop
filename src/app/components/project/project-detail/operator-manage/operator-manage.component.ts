import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SearchAgencyComponent } from '../../../../components/project/project-detail/operator-manage/search-agency/search-agency.component';
import { SearchEmployeeComponent } from '../../../../components/project/project-detail/operator-manage/search-employee/search-employee.component';
import { Employee } from '../../../../models/procurement/employee';
import { DocumentStatus } from '../../../../models/procurement/enum/document-status';
import { Lov } from '../../../../models/procurement/lov';
import { Guideline } from '../../../../models/workflow/guideline';
import { ProjectService } from '../../../../services/project.service';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-operator-manage',
  templateUrl: './operator-manage.component.html',
  styleUrls: ['./operator-manage.component.css'],
})
export class OperatorManageComponent implements OnInit {
  dataList: Guideline[] = new Array<Guideline>();
  employeeMap: Map<string, Employee[]> = new Map();

  constructor(
    private dialogRef: MatDialogRef<OperatorManageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private projectService: ProjectService
  ) {
    this.projectService
      .getGuidelineLevel2(this.data.projectCode)
      .subscribe((_res: any) => {
        this.dataList = _res.map((item: Guideline, index: number) => {
          console.log('item.docStatus: ', item.docStatus);
          console.log('index: ', index);
          if (
            item.docStatus === DocumentStatus.FREEZE ||
            item.docStatus === DocumentStatus.APPROVED ||
            item.docStatus === DocumentStatus.INPROCESS
          ) {
            return { ...item, btnEmployee: true, btnCostCenter: true };
          } else if (
            item.docStatus === DocumentStatus.NEW ||
            item.docStatus === DocumentStatus.READY
          ) {
            if (item.costCenterCode !== null) {
              var costCenter = item.costCenterCode;
              var costCenterName = item.costCenterName;
              this.projectService
                .getEmployeeByCostCenter(costCenter)
                .pipe(take(1))
                .subscribe(
                  (_res: Employee[]) => {
                    this.employeeMap.set(costCenter, _res);
                    console.log('this.employeeMap![costCenter]: ');
                    console.log(this.employeeMap.get(costCenter));
                  },
                  (_err) => {
                    this._snackBar.open(
                      `หน่วยงาน ${costCenter}: ${costCenterName} ไม่มีข้อมูลพนักงานที่เกี่ยวข้องกับ`,
                      'close',
                      {
                        panelClass: ['snackbar-error'],
                        horizontalPosition: 'right',
                        verticalPosition: 'top',
                        duration: 5000,
                      }
                    );
                  }
                );
              return { ...item, btnEmployee: false, btnCostCenter: false };
            }
            return { ...item, btnEmployee: true };
          }
          return { ...item, btnEmployee: true };
        });
        if (this.data.projectStepId != undefined) {
          this.dataList = this.dataList.filter(
            (_item) => _item.code == this.data.projectStepId
          );
        }
      });
  }

  ngOnInit(): void {
    console.log('== ngOnInit ==');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  manageAgency(projectStepId: number) {
    const dialogRef = this.dialog.open(SearchAgencyComponent, {
      width: '70%',
      data: {
        accountSegmentsList: this.data.accountSegmentsList,
      },
    });

    dialogRef.afterClosed().subscribe((result: SelectionModel<Lov>) => {
      console.log(result);
      if (result !== undefined && result.selected.length > 0) {
        this.dataList = this.dataList.map((row) =>
          row.code == projectStepId
            ? {
                ...row,
                costCenterCode: '',
                costCenterName: '',
                employeeNumber: '',
                employeeName: '',
                btnEmployee: true,
              }
            : row
        );
        let selectedItem = result.selected.pop()!;
        let costCenter = selectedItem.code!;
        this.projectService
          .getEmployeeByCostCenter(costCenter)
          .pipe(take(1))
          .subscribe(
            (_res: Employee[]) => {
              this.employeeMap.set(costCenter, _res);
              this.dataList = this.dataList.map((row) =>
                row.code == projectStepId
                  ? {
                      ...row,
                      costCenterCode: costCenter,
                      costCenterName: selectedItem.name!,
                      btnEmployee: false,
                    }
                  : row
              );
            },
            (_err) => {
              this._snackBar.open(
                `หน่วยงาน ${costCenter}: ${selectedItem.name} ไม่มีข้อมูลพนักงานที่เกี่ยวข้องกับ`,
                'close',
                {
                  panelClass: ['snackbar-error'],
                  horizontalPosition: 'right',
                  verticalPosition: 'top',
                  duration: 5000,
                }
              );
              this.dataList = this.dataList.map((row) =>
                row.code == projectStepId
                  ? {
                      ...row,
                      costCenterCode: costCenter,
                      costCenterName: selectedItem.name!,
                      btnEmployee: true,
                    }
                  : row
              );
            }
          );
        return;
      }
      console.log('The dialog was closed');
    });
  }

  manageEmployee(projectStepId: number) {
    let costCenterCode = this.dataList
      .filter((x) => x.code === projectStepId)
      .map((x) => x.costCenterCode);
    console.log('costCenterCode: ', costCenterCode);

    const dialogRef = this.dialog.open(SearchEmployeeComponent, {
      width: '70%',
      data: {
        employeeList: this.employeeMap.get(costCenterCode[0]),
      },
    });

    dialogRef.afterClosed().subscribe((result: SelectionModel<Employee>) => {
      console.log(result);
      if (result !== undefined && result.selected.length > 0) {
        let selectedItem: Employee = result.selected.pop()!;
        this.dataList = this.dataList.map((row) =>
          row.code == projectStepId
            ? {
                ...row,
                employeeNumber: selectedItem.employeeNumber!,
                employeeName: `${
                  selectedItem.title ? selectedItem.title : ''
                } ${selectedItem.firstName ? selectedItem.firstName : ''} ${
                  selectedItem.lastName ? selectedItem.lastName : ''
                }`,
              }
            : row
        );
        return;
      }

      console.log('The dialog was closed');
    });
  }
}
