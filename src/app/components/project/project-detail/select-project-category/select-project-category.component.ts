import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-select-project-category',
  templateUrl: './select-project-category.component.html',
  styleUrls: ['./select-project-category.component.css']
})
export class SelectProjectCategoryComponent implements OnInit {
  selected: boolean = false;
  com: any = 'COM';
  noncom: any = 'NONCOM';

  constructor(private dialogRef: MatDialogRef<SelectProjectCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.selected = data.currentValue === 'NONCOM'
  }

  ngOnInit(): void { console.log("== ngOnInit =="); }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
