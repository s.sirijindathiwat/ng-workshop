import { Component, ViewChild, ElementRef, HostListener, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';
import { AuthService } from '../../services/login/auth.service';
import { UserInfo } from '../../models/userinfo';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  currentUser!: UserInfo;
  opened = true;
  nowDate = new Date();

  //latest_date: string | null;
  //@ViewChild(MatSidenav) sidenav!: MatSidenav;
  @ViewChild('sidenav', { static: true }) sidenav!: MatSidenav;
  //@ViewChild(ElementRef) appDrawer!: ElementRef;
  @ViewChild('appDrawer', { static: true }) appDrawer!: ElementRef;
  public href: string = "";

  constructor(private router: Router, private authenService: AuthService, public _location: Location, private datePipe: DatePipe, private observer: BreakpointObserver) {
    this.authenService.currentUser.subscribe(x => this.currentUser = x);
    this.datePipe.transform(this.nowDate, 'yyyy-MM-dd');
  }
  ngOnInit(): void {
    console.log("== ngOnInit ==");
  }

  // global variable for string interpolation on html
  getCurrentDate() {
    setInterval(() => {
      this.nowDate = new Date(); //set time variable with current date 
    }, 1000); // set it every one seconds}
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  logOut() {
    this.authenService.logout();
    this.router.navigate(['/login']);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event: any) { //: { target: { innerWidth: number; }; }
    if (event.target.innerWidth < 768) {
      //this.sidenav.fixedTopGap = 55;
      this.opened = false;
    } else {
      //this.sidenav.fixedTopGap = 55
      this.opened = true;
    }
  }

  isBiggerScreen() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width < 768) {
      return true;
    } else {
      return false;
    }
  }

}
