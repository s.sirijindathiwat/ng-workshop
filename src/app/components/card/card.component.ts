import { Component } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

  constructor() { }
  titleSelected?: string;
  defaultElevation = 2;
  raisedElevation = 8;
  cards = ['แผนการจัดซื้อจัดจ้าง', 'การจัดหาพัสดุ', 'รายงาน', 'การบริหารสัญญา', 'Administrator'];
  matIcons = ['assignment_ind', 'add_shopping_cart', 'description', 'contacts', 'build'];

  select(num: number) {
    this.titleSelected = num.toString();
  }

}
