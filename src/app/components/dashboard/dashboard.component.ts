import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  centered = false;
  disabled = false;
  unbounded = false;

  radius!: number;
  color!: string;
  constructor() { }

}
