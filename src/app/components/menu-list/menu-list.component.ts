import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../services/login/auth.service';
import { Router } from '@angular/router';
import { UserInfo } from '../../models/userinfo';
import {MENU_CONFIG, MENU_PROCUREMENT, MENU_REPORT} from './menu-procurement';
@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {

  @Output() sidenavClose = new EventEmitter();
  @Output() sivenavToggle = new EventEmitter();
  isAdmin = false;
  isOpApp = false;
  isMaker = false;
  currentUser!: UserInfo;

  menuProcurement: any = MENU_PROCUREMENT;  
  menuConfig: any = MENU_CONFIG;
  menuReport: any = MENU_REPORT;

  constructor(private router: Router,private authenService: AuthService) {
    this.authenService.currentUser.subscribe(x => {
      this.currentUser = x;
      if (x.role == 'PSADM') {
        this.isOpApp = false;
        this.isAdmin = true;
        this.isMaker = false;
      } else if (x.role == 'OPAPP') {
        this.isOpApp = true;
        this.isAdmin = false;
        this.isMaker = false;
      } else {
        this.isOpApp = false;
        this.isAdmin = false;
        this.isMaker = true;
      }
    });
    
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
   }
   
  ngOnInit(): void {
    console.log("== ngOnInit ==");
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }
  public onToggleSidenav = () => {
    this.sivenavToggle.emit();
  }

  public logOut() {
    this.authenService.logout();
    this.router.navigate(['/login']);
  }

}
