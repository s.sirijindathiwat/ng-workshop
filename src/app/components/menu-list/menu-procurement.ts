export const MENU_PROCUREMENT: any = [
    {
        title: 'โครงการ/งาน',
        icon: 'source',
        link: '/projects',
    },
    {
        title: 'BU Checklist',
        icon: 'fact_check',
        link: '/bu-checklist-summary',
    },
    {
        title: 'จัดทำเอกสาร',
        icon: 'drive_file_rename_outline',
        link: '/informations',
    },
    {
      title: 'PR',
      link: '/pr',
      icon: 'request_quote'
    },
    {
      title: 'PO',
      link: '/po',
      icon: 'shopping_cart'
    },
    // {
    //     title: 'login',
    //     link: '/login',
    // },
    // {
    //     title: 'dashboard',
    //     link: '/dashboard',
    // },
    // {
    //     title: 'Planning',
    //     link: '/Planning/N2019-108528-0001/edit',
    // },
    // {
    //     title: 'PlanningDetail',
    //     link: '/PlanningDetail/N2019-108528-0001/edit',
    // },
    // {
    //     title: 'PlanningSummary',
    //     link: '/PlanningSummary',
    // },
    // {
    //     title: 'UserSummary',
    //     link: '/UserSummary',
    // },
    // {
    //     title: 'PlanningDetail',
    //     link: '/PlanningDetail',
    // },
    // {
    //     title: 'Planning',
    //     link: '/Planning',
    // },
    // {
    //     title: 'PlanningAdminSummary',
    //     link: '/PlanningAdminSummary',
    // },
    // {
    //     title: 'PlaningAdminList',
    //     link: '/PlaningAdminList',
    // },
    // {
    //     title: 'queryeditor',
    //     link: '/queryeditor',
    // }
]
export const MENU_INFORMATION: any = [
    {
        title: 'infoSum',
        icon: 'text_snippet',
        link: '/information',
    },
    {
        title: 'info1',
        icon: 'text_snippet',
        link: '/information1',
    },
    {
        title: 'info2',
        icon: 'text_snippet',
        link: '/info2-noncom-page',
    },
    {
        title: 'info3',
        icon: 'text_snippet',
        link: '/information3',
    },

  {
    title: 'info6-1',
    icon: 'text_snippet',
    link: '/information6-1',
  },
  {
    title: 'info6-2',
    icon: 'text_snippet',
    link: '/information6-2',
  },
  {
    title: 'info41',
    icon: 'text_snippet',
    link: '/information41',
  },
]

export const MENU_CONFIG: any = [
  {
    title: 'Document Flow',
    link: '/config-doc-flow',
    icon: 'account_tree'
  },
  {
    title: 'Step',
    link: '/config-step',
    icon: 'next_plan'
  },
  {
    title: 'Document',
    link: '/config-document',
    icon: 'description'
  },
]

export const MENU_REPORT: any = [
  {
    title: 'PR_SUMMARY',
    link: '/prsummary',
    icon: 'description'
  },
  {
    title: 'PO_SUMMARY',
    link: '/posummary',
    icon: 'description'
  },
  {
    title: 'ETC.',
    link: '/etc',
    icon: 'description'
  },
]
