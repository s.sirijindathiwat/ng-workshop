import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

export interface DialogData {
  iconHeader: string;
  header: string;
  detail: string;
  button1: string;
  button2: string;
  data: any;
}

@Component({
  selector: 'app-dialog-master',
  templateUrl: './dialog-master.component.html',
  styleUrls: ['./dialog-master.component.css']
})
export class DialogMasterComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogMasterComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    console.log(this.data)
  }

  closePage() {
    this.dialogRef.close();
  }
}
