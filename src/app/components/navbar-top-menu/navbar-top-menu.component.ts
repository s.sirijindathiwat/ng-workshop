import { Component, Input, OnInit } from '@angular/core';

export interface MenuList {
  title: string;
  link: string;
  isActive: boolean;
}

@Component({
  selector: 'app-navbar-top-menu',
  templateUrl: './navbar-top-menu.component.html',
  styleUrls: ['./navbar-top-menu.component.css']
})
export class NavbarTopMenuComponent implements OnInit {

  @Input() listMenu?: Array<MenuList>;

  constructor() {
  }

  ngOnInit(): void {
    console.log("== ngOnInit ==");
  }

}
