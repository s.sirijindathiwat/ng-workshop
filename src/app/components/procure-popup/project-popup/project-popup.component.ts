import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData } from '../../../components/dialog/dialog-master/dialog-master.component';
import { Paging } from '../../../models/paging';
import { ProjectPopupElement } from '../../../models/popup/project-ds';
import { ProjectService } from '../../../services/project.service';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-project-popup',
  templateUrl: './project-popup.component.html',
  styleUrls: ['./project-popup.component.css'],
})
export class ProjectPopupComponent implements OnInit, OnDestroy, AfterViewInit {
  title: string = 'ค้นหาโครงการ/งาน';
  titleDesc: string = 'รหัส/ชื่อโครงการ';
  // OnDestroy
  destroy$: Subject<boolean> = new Subject<boolean>();

  pageEvent?: PageEvent;
  pageSize: number = 3;
  totalAll: number = 3;
  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  paging?: Paging;

  displayedColumns: string[] = ['select', 'code', 'name'];
  dataSource = new MatTableDataSource<ProjectPopupElement>();
  selection = new SelectionModel<ProjectPopupElement>(false, []);

  btnSearch: boolean = true;
  searchCtrl: FormControl = new FormControl({ value: '', disabled: false }, [
    Validators.required,
    Validators.minLength(4),
  ]);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _snackBar: MatSnackBar,
    private service: ProjectService
  ) {}

  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    console.log(this.data);
    this.searchCtrl.valueChanges.subscribe(
      (checked) => (this.btnSearch = this.searchCtrl.invalid)
    );
  }

  onSearch(): void {
    console.log('--onSearch--');
    if (this.searchCtrl.valid) {
      this.service
        .searchProjectMasterByCodeAndName(
          this.searchCtrl.value,
          this.paginator.pageIndex,
          this.paginator.pageSize
        )
        .pipe(take(1))
        .subscribe(
          (resp: { data: any; paging: Paging }) => {
            if (resp.data.length == 0) {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                duration: 5000,
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            } else {
              var data: Iterable<any> | null | undefined = resp.data;
              var uniq = [...new Set(data)];
              if (uniq.length === 1) {
                this.selection.isSelected(uniq[0]);
                this.selection.toggle(uniq[0]);
              }
              this.dataSource = new MatTableDataSource<ProjectPopupElement>(
                uniq
              );

              this.paging = resp.paging;
              console.log('this.paging?.totalAll!: ', this.paging?.totalAll!);
              this.totalAll = this.paging?.totalAll!;
              this.paginator.length = this.paging?.totalAll!;
              this.paginator.pageSize = this.pageSize;
            }
            // this.paging = resp.paging;
            // console.log("this.paging?.totalAll!: ", this.paging?.totalAll!);
            // this.totalAll = this.paging?.totalAll!;
            // this.paginator.length = this.paging?.totalAll!;
            // this.paginator.pageSize = this.pageSize;
          },
          (_err: Error) => {
            if (_err.message == '2001') {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            }
          }
        );
    }
  }

  public paginatorEvent(e: PageEvent): PageEvent | any {
    console.log(e);
    this.pageSize = e.pageSize;
    this.onSearch();
    return e;
  }
}
