import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData } from '../../../components/dialog/dialog-master/dialog-master.component';
import { Paging } from '../../../models/paging';
import { ProjectBudgetPopupElement } from '../../../models/popup/projectBudget-ds';
import { ProjectService } from '../../../services/project.service';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-budget-popup',
  templateUrl: './budget-popup.component.html',
  styleUrls: ['./budget-popup.component.css'],
})
export class BudgetPopupComponent implements OnInit, OnDestroy, AfterViewInit {
  title: string = 'ค้นหารหัสแผน/ชื่อแผนงาน';
  titleDesc: string = 'รหัสแผน/ชื่อแผนงาน';
  // OnDestroy
  destroy$: Subject<boolean> = new Subject<boolean>();

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  paging?: Paging;
  pageEvent?: PageEvent;
  pageSize: number = 3;
  totalAll: number = 5;
  displayedColumns: string[] = ['select', 'code', 'name'];
  dataSource = new MatTableDataSource<ProjectBudgetPopupElement>();
  selection = new SelectionModel<ProjectBudgetPopupElement>(false);

  btnSearch: boolean = true;
  searchCtrl: FormControl = new FormControl({ value: '', disabled: false }, [
    Validators.required,
    Validators.minLength(4),
  ]);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _snackBar: MatSnackBar,
    private service: ProjectService
  ) {}

  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    console.log(this.data);
    this.searchCtrl.valueChanges.subscribe(
      (checked) => (this.btnSearch = this.searchCtrl.invalid)
    );
    console.log(this.dataSource.data);
  }

  onSearch(): void {
    console.log('--onSearch--');
    if (this.searchCtrl.valid) {
      this.service
        .searchProjectBudgetByCodeAndName(
          this.searchCtrl.value,
          this.paginator.pageIndex,
          this.paginator.pageSize
        )
        .pipe(take(1))
        .subscribe(
          (_res: { data: ProjectBudgetPopupElement[]; paging: Paging }) => {
            if (_res.data.length == 0) {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                duration: 5000,
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            } else {
              var uniq = [...new Set(_res.data)];
              this.paging = _res.paging;
              this.dataSource =
                new MatTableDataSource<ProjectBudgetPopupElement>(uniq);
              console.log('this.paging?.totalAll!: ', this.paging.totalAll!);
              this.totalAll = this.paging?.totalAll!;
              this.paginator.length = this.paging.totalAll!;
              this.paginator.pageSize = this.pageSize;
              if (uniq.length === 1) {
                this.selection.isSelected(uniq[0]);
                this.selection.toggle(uniq[0]);
              }
            }
          },
          (_err: Error) => {
            if (_err.message == '2001') {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            }
          }
        );
    }
  }

  public paginatorEvent(e: PageEvent): PageEvent | any {
    console.log(e);
    this.pageSize = e.pageSize;
    this.onSearch();
    return e;
  }
}
