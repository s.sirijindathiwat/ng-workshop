import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatSelectChange } from '@angular/material/select';
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AccountSegmentsCostcenterPopupComponent } from '@app/components/procure-popup/account-segments-costcenter-popup/account-segments-costcenter-popup.component';
import { UserInfo } from '@app/models/userinfo'
import { AuthService } from '@app/services/login/auth.service'
@Component({
  selector: 'app-info-budget-popup',
  templateUrl: './info-budget-popup.component.html',
  styleUrls: ['./info-budget-popup.component.css']
})
export class InfoBudgetPopupComponent implements OnInit {
  @Input() i: any
  @Input() budgetTypeList: any
  @Output() deleteBudget = new EventEmitter<any>()
  @Output() budgetValue = new EventEmitter<any>()
  @Input() formData!: FormGroup
  @Input() isDim: boolean = false
  // form!: FormGroup;

  id: any
  date = new FormControl(new Date())
  date2 = new FormControl(new Date())

  currentUser!: UserInfo;
  constructor(private fb: FormBuilder,private dialog: MatDialog,private _snackBar: MatSnackBar,private authenService: AuthService,) {}

  ngOnInit(): void {
    if(!this.formData.controls['budgetDesc1'].value){
      this.authenService.currentUser.subscribe(x => {
            this.currentUser = x ;
            this.formData.controls['costCenterCode'].setValue(x.costCenterCode) 
            this.formData.controls['costCenterName'].setValue(x.costCenterCode + ": " + x.costCenterDescr)
            this.formData.controls['budgetDesc1'].setValue(x.costCenterCode + ": " + x.costCenterDescr)
          });
    }else{
      this.formData.controls['costCenterName'].setValue(this.formData.controls['budgetDesc1'].value)
  
    }
    this.setFrom()
  }

  setFrom() {
    if (this.formData.value.budgetYear !== null && this.formData.value.budgetYear !== '') {
      let budgetYear = new Date('01/01/' + this.formData.value.budgetYear)
      console.log(budgetYear)
      this.formData.get('budgetYear')?.setValue(budgetYear)
    }

    if (this.formData.value.forwardBudgetYear !== null && this.formData.value.forwardBudgetYear !== '') {
      let forwardBudgetYear = new Date('01/01/' + this.formData.value.forwardBudgetYear)
      console.log(forwardBudgetYear)
      this.formData.get('forwardBudgetYear')?.setValue(forwardBudgetYear)
    }
    this.setRequireField(this.formData.value.budgetType)
  }

  onDeleteBudget(index: any) {
    console.log(index)
    if (index > -1) {
      this.formData.removeControl(this.id)
      this.deleteBudget.emit(index)
    }
  }

  chooseYearHandler(normalizedYear: Date, datepicker: MatDatepicker<any>) {
    this.date.setValue(normalizedYear)
    this.formData.get('budgetYear')?.setValue(normalizedYear)
    console.log(normalizedYear)
    datepicker.close()
  }

  chooseForwardBudgetYearHandler(normalizedYear: Date, datepicker: MatDatepicker<any>) {
    this.date2.setValue(normalizedYear)
    this.formData.get('forwardBudgetYear')?.setValue(normalizedYear)
    console.log(normalizedYear)
    datepicker.close()
  }

  changeBudgetType($event: MatSelectChange) {
    console.log($event)
    // this.form.controls['budgetYear'].setValue('');//2 11
    // this.form.controls['budgetDesc1'].setValue('');//3 12
    // this.form.controls['budgetDesc2'].setValue('');//4 13
    // this.form.controls['budgetAmount'].setValue('');//5 14
    // this.form.controls['budgetRemain'].setValue('');//6 15
    // this.form.controls['budgetDesc3'].setValue('');//7 16
    // this.form.controls['forwardBudgetYear'].setValue('');//8 17
    // this.form.controls['forwardBudgetDate'].setValue('');//9 18
    // this.form.controls['forwardBudgetDoc'].setValue('');//10 19
    // this.form.controls['forwardBudgetEmailDate'].setValue('');//11 20
    // this.form.controls['budgetDesc4'].setValue('');//12 21

    if ($event.value === '11') {
      this.formData.controls['budgetDesc3'].setValue('') //7
      this.formData.controls['forwardBudgetYear'].setValue('') //8
      this.formData.controls['forwardBudgetDate'].setValue(undefined) //9
      this.formData.controls['forwardBudgetDoc'].setValue('') //10
      this.formData.controls['forwardBudgetEmailDate'].setValue(undefined) //11
      this.formData.controls['budgetDesc4'].setValue('') //11 21
    } else if ($event.value === '12' || $event.value === '13' || $event.value === '14' || $event.value === '17') {
      this.formData.controls['budgetRemain'].setValue(0) //6
      this.formData.controls['budgetDesc3'].setValue('') //7
      this.formData.controls['forwardBudgetYear'].setValue('') //8
      this.formData.controls['forwardBudgetDate'].setValue(undefined) //9
      this.formData.controls['forwardBudgetDoc'].setValue('') //10
      this.formData.controls['forwardBudgetEmailDate'].setValue(undefined) //11
      this.formData.controls['budgetDesc4'].setValue('') //11 21
    } else if ($event.value === '15') {
      this.formData.controls['budgetRemain'].setValue(0) //6
      this.formData.controls['forwardBudgetYear'].setValue('') //8
      this.formData.controls['forwardBudgetDate'].setValue(undefined) //9
      this.formData.controls['forwardBudgetDoc'].setValue('') //10
      this.formData.controls['forwardBudgetEmailDate'].setValue(undefined) //11
    } else if ($event.value === '16') {
      this.formData.controls['budgetYear'].setValue('') //2
      this.formData.controls['budgetDesc2'].setValue('') //4
      this.formData.controls['budgetRemain'].setValue(0) //6
      this.formData.controls['budgetDesc3'].setValue('') //7
      this.formData.controls['forwardBudgetYear'].setValue('') //8
      this.formData.controls['forwardBudgetDate'].setValue(undefined) //9
      this.formData.controls['forwardBudgetDoc'].setValue('') //10
      this.formData.controls['forwardBudgetEmailDate'].setValue(undefined) //11
      this.formData.controls['budgetDesc4'].setValue('') //11 21
    } else if ($event.value === '18') {
      this.formData.controls['budgetRemain'].setValue(0) //6
      this.formData.controls['budgetDesc3'].setValue('') //7
      this.formData.controls['forwardBudgetDoc'].setValue('') //10
      this.formData.controls['forwardBudgetEmailDate'].setValue(undefined) //11
      this.formData.controls['budgetDesc4'].setValue('') //11 21
    } else if ($event.value === '19') {
      this.formData.controls['budgetRemain'].setValue(0) //6
      this.formData.controls['budgetDesc3'].setValue('') //7
      this.formData.controls['budgetDesc4'].setValue('') //11 21
    } else if ($event.value === '20') {
      this.formData.controls['budgetYear'].setValue('') //2
      this.formData.controls['budgetDesc2'].setValue('') //4
      this.formData.controls['budgetAmount'].setValue(0) //5
      this.formData.controls['budgetRemain'].setValue(0) //6
      this.formData.controls['budgetDesc3'].setValue('') //7
      this.formData.controls['forwardBudgetYear'].setValue('') //8
      this.formData.controls['forwardBudgetDate'].setValue(undefined) //9
      this.formData.controls['forwardBudgetDoc'].setValue('') //10
      this.formData.controls['forwardBudgetEmailDate'].setValue(undefined) //11
      this.formData.controls['budgetDesc4'].setValue('') //11 21
    }
    this.setRequireField($event.value)
  }

  setRequireField(value: any) {
    if (value === '11') {
      this.clearValidators()
      this.formData.controls['budgetYear'].setValidators([Validators.required]) //2
      this.formData.controls['budgetDesc1'].setValidators([Validators.required]) //3
      this.formData.controls['budgetDesc2'].setValidators([Validators.required]) //4
      this.formData.controls['budgetAmount'].setValidators([Validators.required]) //5
      this.formData.controls['budgetRemain'].setValidators([Validators.required]) //6

    
      this.updateValueAndValidity()
    } else if (value === '12' || value === '13' || value === '14' || value === '17') {
      this.clearValidators()
      this.formData.controls['budgetYear'].setValidators([Validators.required]) //2
      this.formData.controls['budgetDesc1'].setValidators([Validators.required]) //3
      this.formData.controls['budgetDesc2'].setValidators([Validators.required]) //4
      this.formData.controls['budgetAmount'].setValidators([Validators.required]) //5
      this.updateValueAndValidity()
    } else if (value === '15') {
      this.clearValidators()
      this.formData.controls['budgetYear'].setValidators([Validators.required]) //2
      this.formData.controls['budgetDesc1'].setValidators([Validators.required]) //3
      this.formData.controls['budgetDesc2'].setValidators([Validators.required]) //4
      this.formData.controls['budgetAmount'].setValidators([Validators.required]) //5
      this.formData.controls['budgetDesc3'].setValidators([Validators.required]) //7
      this.formData.controls['budgetDesc4'].setValidators([Validators.required]) //7
      this.updateValueAndValidity()
    } else if (value === '16') {
      this.clearValidators()
      this.formData.controls['budgetDesc1'].setValidators([Validators.required]) //3
      this.formData.controls['budgetAmount'].setValidators([Validators.required]) //5
      this.updateValueAndValidity()
    } else if (value === '18') {
      this.clearValidators()
      this.formData.controls['budgetYear'].setValidators([Validators.required]) //2
      this.formData.controls['budgetDesc1'].setValidators([Validators.required]) //3
      this.formData.controls['budgetDesc2'].setValidators([Validators.required]) //4
      this.formData.controls['budgetAmount'].setValidators([Validators.required]) //5
      this.formData.controls['forwardBudgetYear'].setValidators([Validators.required]) //8
      this.formData.controls['forwardBudgetDate'].setValidators([Validators.required]) //9
      this.updateValueAndValidity()
    } else if (value === '19') {
      this.clearValidators()
      this.formData.controls['budgetYear'].setValidators([Validators.required]) //2
      this.formData.controls['budgetDesc1'].setValidators([Validators.required]) //3
      this.formData.controls['budgetDesc2'].setValidators([Validators.required]) //4
      this.formData.controls['budgetAmount'].setValidators([Validators.required]) //5
      this.formData.controls['forwardBudgetYear'].setValidators([Validators.required]) //8
      this.formData.controls['forwardBudgetDate'].setValidators([Validators.required]) //9
      this.formData.controls['forwardBudgetDoc'].setValidators([Validators.required]) //10
      this.formData.controls['forwardBudgetEmailDate'].setValidators([Validators.required]) //11
      this.updateValueAndValidity()
    } else if (value === '20') {
      this.clearValidators()
      this.formData.controls['budgetDesc1'].setValidators([Validators.required]) //3
      this.updateValueAndValidity()
    }
  }
  clearValidators() {
    for (var formName in this.formData.controls) {
      this.formData.controls[formName].clearValidators()
      this.formData.controls[formName].updateValueAndValidity()
    }
  }
  updateValueAndValidity() {
    for (var formName in this.formData.controls) {
      this.formData.controls[formName].updateValueAndValidity()
    }
  }
  clearDatepicker(formControlName: string) {
    this.formData.controls[formControlName].setValue(null);
  }

  onFindCostCenter() { 
    const dialogRef = this.dialog.open(AccountSegmentsCostcenterPopupComponent, {
      width: '800px',
      data: {}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.length > 0) { 
          this.formData.controls['costCenterCode'].setValue(result[0].code) 
          this.formData.controls['costCenterName'].setValue(result[0].code + ": " + result[0].name)
          this.formData.controls['budgetDesc1'].setValue(result[0].code + ": " + result[0].name)
        } else {
          this.formData.controls['costCenterCode'].setValue(null) 
          this.formData.controls['costCenterName'].setValue(null) 
          this.formData.controls['budgetDesc1'].setValue(null)
        }
      }
    }, _err => {
      this._snackBar.open(_err, 'close', {
        panelClass: ['snackbar-error'],
        horizontalPosition: "right",
        verticalPosition: "top",
        duration: 5000,
      });
    });
     
  }
}


