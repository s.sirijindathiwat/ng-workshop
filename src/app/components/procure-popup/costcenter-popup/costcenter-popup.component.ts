import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData } from '../../../components/dialog/dialog-master/dialog-master.component';
import { Paging } from '../../../models/paging';
import { CostCenterPopupElement } from '../../../models/popup/costcenter-ds';
import { Lov } from '../../../models/procurement/lov';
import { ProjectService } from '../../../services/project.service';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-costcenter-popup',
  templateUrl: './costcenter-popup.component.html',
  styleUrls: ['./costcenter-popup.component.css'],
})
export class CostcenterPopupComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  title: string = 'ค้นหาหน่วยงาน';
  titleDesc: string = 'รหัส/ชื่อหน่วยงาน';
  // OnDestroy
  destroy$: Subject<boolean> = new Subject<boolean>();

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  paging?: Paging;
  pageEvent?: PageEvent;
  pageSize: number = 3;
  totalAll: number = 5;
  displayedColumns: string[] = ['select', 'code', 'name'];
  dataSource = new MatTableDataSource<CostCenterPopupElement>();
  selection = new SelectionModel<CostCenterPopupElement>(false, []);

  btnSearch: boolean = true;
  searchCtrl: FormControl = new FormControl({ value: '', disabled: false }, [
    Validators.required,
    Validators.minLength(4),
  ]);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _snackBar: MatSnackBar,
    private service: ProjectService
  ) {}

  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    console.log(this.data);
    this.searchCtrl.valueChanges.subscribe(
      (checked) => (this.btnSearch = this.searchCtrl.invalid)
    );
  }

  onSearch(): void {
    console.log('--onSearch--');
    if (this.searchCtrl.valid) {
      this.service
        .getAccountSegmentsList()
        .pipe(take(1))
        .subscribe(
          (_res: Lov[]) => {
            if (_res.length == 0) {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                duration: 5000,
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            } else {
              let lovList: CostCenterPopupElement[] = _res
                .filter(
                  (item) =>
                    item.code?.includes(this.searchCtrl.value) ||
                    item.name?.includes(this.searchCtrl.value)
                )
                .map((_lov: Lov) => ({
                  code: _lov.code!,
                  name: _lov.name!,
                }));
              this.dataSource = new MatTableDataSource<CostCenterPopupElement>(
                lovList
              );
              if (lovList.length === 1) {
                this.selection.isSelected(lovList[0]);
                this.selection.toggle(lovList[0]);
              }
              this.totalAll = lovList.length!;
              this.paginator.length = this.totalAll;
              this.paginator.pageSize = this.totalAll;
            }
          },
          (_err: Error) => {
            if (_err.message == '2001') {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            }
          }
        );
    }
  }

  public paginatorEvent(e: PageEvent): PageEvent | any {
    console.log(e);
    this.pageSize = e.pageSize;
    this.onSearch();
    return e;
  }
}
