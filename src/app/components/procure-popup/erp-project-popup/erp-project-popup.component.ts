import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData } from '../../../components/dialog/dialog-master/dialog-master.component';
import { Paging } from '../../../models/paging';
import {
  SearchErpProjectPopupElement,
  SearchErpProjectResponse,
} from '../../../models/project-mst/erp-project';
import { ProjectService } from '../../../services/project.service';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-erp-project-popup',
  templateUrl: './erp-project-popup.component.html',
  styleUrls: ['./erp-project-popup.component.css'],
})
export class ErpProjectPopupComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  title: string = 'ค้นหาโครงการ/งาน';
  titleDesc: string = 'รหัส/ชื่อโครงการ';
  // OnDestroy
  destroy$: Subject<boolean> = new Subject<boolean>();

  pageEvent?: PageEvent;
  pageSize: number = 3;
  totalAll: number = 3;
  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  paging?: Paging;
  displayedColumns: string[] = [
    'select',
    'projectId',
    'segment1',
    'description',
  ];
  dataSource = new MatTableDataSource<SearchErpProjectPopupElement>();
  selection = new SelectionModel<SearchErpProjectPopupElement>(false, []);

  btnSearch: boolean = true;
  searchCtrl: FormControl = new FormControl({ value: '', disabled: false });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _snackBar: MatSnackBar,
    private service: ProjectService
  ) {}

  ngOnDestroy(): void {
    console.log('== ngOnDestroy ==');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    console.log(this.data);
    this.searchCtrl.valueChanges.subscribe(
      (checked) => (this.btnSearch = this.searchCtrl.invalid)
    );
  }

  onSearch(): void {
    console.log('--onSearch--');
    if (this.searchCtrl.valid) {
      this.service
        .searchErpProject(
          this.searchCtrl.value,
          this.paginator.pageIndex,
          this.paginator.pageSize
        )
        .pipe(take(1))
        .subscribe(
          (resp: SearchErpProjectResponse) => {
            console.log(resp.data);
            if (resp.data.length !== 0) {
              var data: Iterable<any> | null | undefined = resp.data;
              var uniq = [...new Set(data)];
              this.dataSource =
                new MatTableDataSource<SearchErpProjectPopupElement>(uniq);

              this.paging = resp.paging;
              console.log('this.paging?.totalAll!: ', this.paging?.totalAll!);
              this.totalAll = this.paging?.totalAll!;
              this.paginator.length = this.paging?.totalAll!;
              this.paginator.pageSize = this.pageSize;
            } else {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            }
          },
          (_err: Error) => {
            if (_err.message == '2001') {
              this._snackBar.open('ไม่พบข้อมูล', 'close', {
                panelClass: ['snackbar-warning'],
                horizontalPosition: 'right',
                verticalPosition: 'top',
              });
            }
          }
        );
    }
  }

  public paginatorEvent(e: PageEvent): PageEvent | any {
    console.log(e);
    this.pageSize = e.pageSize;
    this.onSearch();
    return e;
  }
}
