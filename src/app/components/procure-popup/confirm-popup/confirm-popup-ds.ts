export interface ConfirmPopupData {
    dialogTitle?: string
    title?: string
    description?: string
    btnTrueDescription?: string
    btnFalseDescription?: string
}