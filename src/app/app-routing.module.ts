import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProjectSummaryComponent } from './components/project/project-summary/project-summary.component';
import { ProjectDetailComponent } from './components/project/project-detail/project-detail.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'project-summary', component: ProjectSummaryComponent },
  { path: 'project-detail', component: ProjectDetailComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'projects',
    children: [
      {
        path: '',
        component: ProjectSummaryComponent,
      },
      {
        path: 'create',
        component: ProjectDetailComponent,
      },
      {
        path: ':id',
        component: ProjectDetailComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
