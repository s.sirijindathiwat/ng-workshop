import { Directive } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material/core";
import { AppDateAdapter } from "../shared/date-adapter";

export const APP_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'input',
    monthYearLabel: 'input',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'DD/MMM/YYYY',
  },
};
export const APP_DATE_FORMAT_YYYY_BC = {
  parse: {
    dateInput: 'inputYearBC',
  },
  display: {
    dateInput: 'inputYearBC',
    monthYearLabel: 'inputYearBC',
    dateA11yLabel: 'inputYearBC',
    monthYearA11yLabel: 'inputYearBC',
  },
};
export const APP_DATE_FORMAT_YYYY = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'inputYear',
    monthYearLabel: 'inputYear',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'DD/MMM/YYYY',
  },
};
@Directive({
  selector: '[appDate]'
})
export class DateDirective {

  constructor() { }

}

@Directive({
  selector: '[appDateFormatYYYY]',
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMAT_YYYY },
  ],
})
export class DateDirectiveFormatYYYYDirective {
}

@Directive({
  selector: '[appDateFormatBCYYYY]',
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMAT_YYYY_BC },
  ],
})
export class DateDirectiveFormatYYYYBCDirective {
}
