import { Directive, ElementRef, forwardRef, HostListener, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MAT_INPUT_VALUE_ACCESSOR } from "@angular/material/input";

@Directive({
  selector: 'input[appMatInputCommified]',
  providers: [
    { provide: MAT_INPUT_VALUE_ACCESSOR, useExisting: MatInputCommifiedDirective },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MatInputCommifiedDirective),
      multi: true,
    }
  ]
})
export class MatInputCommifiedDirective {
  // tslint:disable-next-line:variable-name
  _value: string | null = '';

  constructor(private elementRef: ElementRef<HTMLInputElement>,
  ) {
    // console.log('created directive');
  }


  get value(): string | null {
    // console.log(this._value)
    return this._value;
  }

  @Input()
  set value(value: string | null) {
    // console.log(this._value)
    this._value = value;
    this.formatValue(value);
  }
  numberWithCommas(x: any) {
    // console.log(x)
    var parts = x.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
  }
  private formatValue(value: string | null) {
    // console.log(value)
    // console.log(this.elementRef.nativeElement.value)
    if (value !== null && value !== '' && value !== undefined) {
      this.elementRef.nativeElement.value = this.numberWithCommas(parseFloat(value).toFixed(2));
    } else {
      this.elementRef.nativeElement.value = parseFloat('0').toFixed(2);
    }
  }

  private unFormatValue() {
    const value = this.elementRef.nativeElement.value;
    this._value = value.replace(/[^\d.]/g, '');
    if (value) {
      this.elementRef.nativeElement.value = this._value;
    } else {
      this.elementRef.nativeElement.value = '';
    }
  }

  @HostListener('input', ['$event.target.value'])
  onInput(value: any) {
    // console.log(value)
    this._value = value.replace(/[^\d.]/g, '');
    // console.log(this._value)

    if (this._value === '' || this._value === undefined || this._value === null) {
      this._onChange(null);
    } else {
      if (typeof this._value === "string") {
        this._onChange(Number(parseFloat(this._value).toFixed(2)));
      }
    } // here to notify Angular Validators
  }

  @HostListener('blur')
  _onBlur() {
    this.formatValue(this._value);
  }

  @HostListener('focus')
  onFocus() {
    this.unFormatValue();
  }

  _onChange(value: any): void {
  }

  writeValue(value: any) {
    this._value = value;
    this.formatValue(this._value); // format Value
  }

  registerOnChange(fn: (value: any) => void) {
    this._onChange = fn;
  }

  registerOnTouched() {
  }

}
