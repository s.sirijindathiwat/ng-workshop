import { Injectable } from '@angular/core';
import {
  HttpInterceptor, HttpHandler, HttpRequest, HttpResponse,
  HttpErrorResponse,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpUserEvent
} from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, timer } from 'rxjs'; 
import { finalize, catchError, switchMap, filter, take, delay, retryWhen, tap, mergeMap } from "rxjs/operators";
import { AuthService } from '../services/login/auth.service';
import { Router } from '@angular/router';
import { UserInfo } from '../models/userinfo';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../services/loader.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  //tokenExpired: HttpErrorResponse;

  constructor(private router: Router, private authService: AuthService, public toasterService: ToastrService, public loaderService: LoaderService) { } //public loaderService: LoaderService,

  isRefreshingToken: boolean = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');

  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
    this.loaderService.show();
    return next.handle(this.addTokenToRequest(request, this.authService.getAuthToken()))
      .pipe(
        retryWhen(this.genericRetryStrategy({ maxRetryAttempts: 10, scalingDuration: 500, excludedStatusCodes: [401] })),
        catchError(err => {
          let errMsg: string;
          let errStr: string;
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
                if (request.url.includes('authenticate')) {
                  // console.log(request);
                  this.toasterService.error('รหัสผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง', 'แจ้งเตือน');
                  break;
                }
                else {
                  return this.handle401Error(request, next);
                }
              //return this.handle401Error(request, next);
              case 400:
                this.router.navigate(['/login']);
                return <any>this.authService.logout();
              case 404:
              case 415:
                if (err.url?.includes('RefreshToken')) {
                  this.router.navigate(['/login']);
                  return <any>this.authService.logout();
                }
                break;
            }
            if (err.error != null && err.error.valueOf() instanceof Object) {

              errStr = err.error.title;
            } else if (err.status == 500) {
              errStr = err.message;
            }
            else {
              errStr = err.error;
            }
            errMsg = err.status == 0 ? err.message : errStr;
            this.toasterService.warning(`error code: ${err.status}, Message: ${err.message}`, err.statusText);
            return throwError(err);
          } else {
            return throwError(err);
          }
        })
        , finalize(() => {
          this.loaderService.hide();
        })
      );
  }

  private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {

    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next('');

      return this.authService.refreshToken()
        .pipe(
          switchMap((user: UserInfo) => {
            if (user) {
              this.tokenSubject.next(user.token);;
              localStorage.setItem('currentUser', JSON.stringify(user));
              return next.handle(this.addTokenToRequest(request, user.token));
            }
            return <any>this.authService.logout();
          }),
          catchError(err => {
            return <any>this.authService.logout();
          }),
          finalize(() => {
            this.isRefreshingToken = false;
          })
        );
    } else {
      this.isRefreshingToken = false;
      return this.tokenSubject
        .pipe(filter(token => token != null),
          take(1),
          switchMap(token => {
            return next.handle(this.addTokenToRequest(request, token));
          }));
    }
  }

  private genericRetryStrategy = ({
    maxRetryAttempts = 3,
    scalingDuration = 1000,
    excludedStatusCodes = []
  }: {
    maxRetryAttempts?: number,
    scalingDuration?: number,
    excludedStatusCodes?: number[]
  } = {}) => (attempts: Observable<any>) => {
    return attempts.pipe(
      mergeMap((error, i) => {
        const retryAttempt = i + 1;
        // if maximum number of retries have been met
        // or response is a status code we don't wish to retry, throw error
        if (
          retryAttempt > maxRetryAttempts ||
          excludedStatusCodes.find(e => e === error.status)
        ) {
          return throwError(error);
        }
        console.log(
          `Attempt ${retryAttempt}: retrying in ${retryAttempt *
          scalingDuration}ms`
        );
        // retry after 1s, 2s, etc...
        return timer(retryAttempt * scalingDuration);
      }),
      finalize(() => console.log('We are done!'))
    );
  };
}
