import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { NavbarTopMenuComponent } from '../components/navbar-top-menu/navbar-top-menu.component';
import { MatCardModule } from '@angular/material/card';
import { ReplaceNullWithTextPipe } from '../pipes/replace-null-with-text.pipe';
import { FormsModule } from '@angular/forms';
import { ThaidatePipe } from '../pipes/dateformat/thaidate.pipe';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
  declarations: [ 
    NavbarTopMenuComponent,
    ReplaceNullWithTextPipe,
    ThaidatePipe
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
    RouterModule
  ],
  exports: [ 
    NavbarTopMenuComponent,
    ReplaceNullWithTextPipe,
    ThaidatePipe
  ]
})
export class SharedModule { }
