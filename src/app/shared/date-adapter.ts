import {NativeDateAdapter} from '@angular/material/core';
import {Injectable} from "@angular/core";

@Injectable()
export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    date.setUTCHours(date.getUTCHours() + 7, 0, 0);
    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year;
      if (date.getFullYear() < 2500)
        year = date.getFullYear() + 543;
      else
        year = date.getFullYear();
      return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
    } else if (displayFormat == "inputYear") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year;
      if (date.getFullYear() < 2500)
        year = date.getFullYear() + 543;
      else
        year = date.getFullYear();
      return "" + year;
    } else if (displayFormat == "inputYearBC") {
      return date.getFullYear().toString();
    } else {
      let day = date.getMonth() + 1;
      let month = date.getDate();
      let year;
      if (date.getFullYear() < 2500)
        year = date.getFullYear() + 543;
      else
        year = date.getFullYear();

      return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
    }
  }

  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }
}
